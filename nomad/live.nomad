# This should best be equal to project name, and also the binary name without extension. This defines the ${NOMAD_JOB_NAME}
job "pdf-generator-qmsl-v2" {
  datacenters = ["rwth-app-live"]
  namespace   = "qmsl"
  type        = "service"

  update {
    stagger      = "60s"
    max_parallel = 1
  }

  # Should probably be equal to the Job Name in most cases. This defines the ${NOMAD_GROUP_NAME}
  group "pdf-generator-qmsl-v2" {
    count = 2

    network {
      # This way Nomad will designate a single, random, port named http to the service
      port "http" {}
    }


    # Should probably be equal to the Job/Group Name in most cases. This defines the ${NOMAD_TASK_NAME}
    task "pdf-generator-qmsl-v2" {

      driver = "raw_exec"

      config {
        command = "dotnet"

        # This needs to point to the binary, and config file, relative or absolute.
        # Currently raw_exec will execute everything directly from the created alloc dir ${NOMAD_ALLOC_DIR},
        # but the content will be placed in the /local subdirectory {$NOMAD_TASK_DIR}
        args = [
          "local/output/PDSLLabs.Public.QMSL.PDFGenerator.dll",
          "local/output/appsettings.json"
        ]
      }

      env {
        # Use all IP addresses and port set by Nomad
        ASPNETCORE_URLS = "http://*:${NOMAD_PORT_http}",

        # Pass the basedir used by Traefik to the app
        ASPNETCORE_BASEPATH = "/${NOMAD_TASK_NAME}"
      }

      service {
        name = "${NOMAD_TASK_NAME}"
        port = "http"
        tags = [
          "traefik.enable=true",
          "traefik.http.routers.pdfgeneratorqmslv2.entrypoints=internal",
          "traefik.Path=/${NOMAD_TASK_NAME}"
        ]

        # The check URL needs to answer with status 200, preferbly use something like a version API call
        check {
          type     = "http"
          path     = "/swagger/index.html"
          interval = "10s"
          timeout  = "2s"
        }
      }

      artifact {
        # This will be filled by the Gitlab runner script
        source = "{{ artifact_url }}"
      }
    }
  }
}