# PDFGenerator

Dieses Repository ermöglicht die Generierung von PDF-Dokumenten über einen Microservice via Web API. Zum Testen von Anfragen per Dateneingabe wird eine webbasierte Benutzeroberfläche mit Hilfe des _Swagger_-Tools bereitgestellt.

Grundsätzlich werden befüllte Datenmodelle über einen Webservice an einen PDFGenerator-Microservice gesendet, der dafür sorgt, PDF-Dateien aus den empfangengen Datenmodellen zu erstellen. Vom Webserver wird dann nach aufgerufenem URI das entsprechende PDF-Dokument generiert und als byte Array zurückgegeben.

Die Microservice Server Web API generiert PDF-Dokumente mit Hilfe der verwendeten _iText7_-Bibliothek. _iText_ ist eine freie Programmbibliothek zur Erzeugung und Bearbeitung von PDF-Dateien mittels der Programmiersprachen _Java_ oder _C#_, deren Software unter der [GNU Affero General Public License](https://de.wikipedia.org/wiki/GNU_Affero_General_Public_License) (AGPL) vertrieben wird.

## Einstellung

- TargetFramework: .NET Core 3.1
- iText7 (7.1.12)
- Swashbuckle.AspNetCore (5.6.3)
- NLog (4.7.5)

## Datenmodelle

Zu den Bestandteilen eines PDF-Dokuments zählen je nach Datenmodell möglicherweise ein Deckblatt, ein Vorwort, eine Kopf- und/oder Fußzeile, Kapitel, Abschnitte sowie ein Anhang. Im Folgenden wird ein typisches Datenmodell-Muster angezeigt:

```csharp
public class PDFDocument
{
    public DataType Header { get; set; }
    public DataType Footer { get; set; }
    public DataType Cover { get; set; }
    public DataType Preface { get; set; }
    public DataType Chapters { get; set; }
    // (...)
    public DataType Appendix { get; set; }
}
```

Die verwendeten Datenmodelle müssen dem PDF-Generator im Verzeichnis `bin` als dll zur Verfügung gestellt werden.

## Generierungsablauf

Zur Generierung des jeweiligen PDF-Dokuments liegt ein _Controller_ vor, in dem sich die vom _WebClient_ aufgerufene Methode befindet. Darin wird ein _Renderer_ mit dem übergebenen befüllten Datenmodell aus dem Request-Body instantiiert, anhand dessen Methode _Render()_ das PDF-Dokument erstellt werden kann. Der _Renderer_ muss dafür das Interface _IRenderer_ implementieren. Dabei wird das Datenmodell-Objekt verwendet, durchiteriert und dadurch jeder Bestandteil ins Dokument geschrieben.

Zuletzt wird das fertige Dokument als byte Array zurückgegeben.