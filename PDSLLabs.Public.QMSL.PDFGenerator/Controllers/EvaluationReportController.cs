using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDSLLabs.Public.QMSL.PDFGenerator.Renderers;
using EvaluationReportModel = PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport.EvaluationReport;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EvaluationReportController : ControllerBase
    {
        private ILogger<EvaluationReportController> Logger { get; }

        public EvaluationReportController(ILogger<EvaluationReportController> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Say hello!
        /// </summary>
        /// <returns>"Hello"</returns>
        [HttpGet("Hello")]
        public string Get()
        {
            return "Hello";
        }

        /// <summary>
        /// Generates the PDF for an evaluation report.
        /// </summary>
        /// <param name="evaluationReport">Evaluation report for which the PDF should be rendered.</param>
        /// <returns>PDF document as byte array.</returns>
        [HttpPost("RenderReport")]
        [RequestSizeLimit(100000000)]
        public byte[] RenderReport([FromBody] EvaluationReportModel evaluationReport)
        {
            Logger.Log(LogLevel.Debug, "Start rendering pdf report.");

            IRenderer Renderer = new EvaluationReportRenderer(evaluationReport);
            byte[] file = Renderer.Render();

            Logger.Log(LogLevel.Debug, "Finished rendering pdf report.");
            return file;
        }
    }
}