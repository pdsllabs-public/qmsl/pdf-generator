using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDSLLabs.Public.QMSL.PDFGenerator.Renderers;
using PDSLLabs.QMSL.PDFGenerator.Models.MeasurePdf;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MeasurePdfController : ControllerBase
    {
        private ILogger<MeasurePdfController> Logger { get; }

        public MeasurePdfController(ILogger<MeasurePdfController> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Say hello!
        /// </summary>
        /// <returns>"Hello"</returns>
        [HttpGet("Hello")]
        public string Get()
        {
            return "Hello";
        }

        /// <summary>
        /// Generates the PDF for an evaluation report.
        /// </summary>
        /// <param name="pdfMeasureTable">Measure table for which the PDF should be rendered.</param>
        /// <returns>PDF document as byte array.</returns>
        [HttpPost("RenderMeasureTable")]
        [RequestSizeLimit(100000000)]
        public byte[] RenderReport([FromBody] PdfMeasureTable pdfMeasureTable)
        {
            Logger.Log(LogLevel.Debug, "Start rendering pdf measure table.");

            IRenderer Renderer = new MeasurePdfRenderer(pdfMeasureTable);
            byte[] file = Renderer.Render();

            Logger.Log(LogLevel.Debug, "Finished rendering pdf measure table.");
            return file;
        }
    }
}