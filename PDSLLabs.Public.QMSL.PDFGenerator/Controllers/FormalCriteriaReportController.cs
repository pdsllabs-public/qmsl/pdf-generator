﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDSLLabs.Public.QMSL.PDFGenerator.Renderers;
using FormalCriteriaReport = PDSLLabs.QMSL.PDFGenerator.Models.FormalCriteriaReport.FormalCriteriaReport;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FormalCriteriaReportController : ControllerBase
    {
        private ILogger<FormalCriteriaReportController> Logger { get; }

        public FormalCriteriaReportController(ILogger<FormalCriteriaReportController> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Say hello!
        /// </summary>
        /// <returns>"Hello"</returns>
        [HttpGet("Hello123")]
        public string Get()
        {
            return "Hello123";
        }

        /// <summary>
        /// Generates the PDF for a formal .
        /// </summary>
        /// <param name="formalCriteriaReport">formalCriteriaReport for which the PDF should be rendered.</param>
        /// <returns>PDF document as byte array.</returns>
        [HttpPost("RenderFormalCriteriaReport")]
        [RequestSizeLimit(100000000)]
        public byte[] RenderFormalCriteriaReport([FromBody] FormalCriteriaReport formalCriteriaReport)
        {
            Logger.Log(LogLevel.Debug, "Start rendering pdf for the Fact Sheet.");

            IRenderer Renderer = new FormalCriteriaReportRenderer(formalCriteriaReport);
            byte[] file = Renderer.Render();

            Logger.Log(LogLevel.Debug, "Finished rendering pdf for the Fact Sheet.");
            return file;
        }

    }
}
