﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDSLLabs.Public.QMSL.PDFGenerator.Renderers;
using FactSheetModel = PDSLLabs.QMSL.PDFGenerator.Models.FactSheet.FactSheet;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class FactSheetController : ControllerBase
    {
        private ILogger<FactSheetController> Logger { get; }

        public FactSheetController(ILogger<FactSheetController> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Say hello!
        /// </summary>
        /// <returns>"Hello"</returns>
        [HttpGet("Hello")]
        public string Get()
        {
            return "Hello";
        }

        /// <summary>
        /// Generates the PDF for a factSheet.
        /// </summary>
        /// <param name="factSheet">FactSheet for which the PDF should be rendered.</param>
        /// <returns>PDF document as byte array.</returns>
        [HttpPost("RenderFactSheet")]
        [RequestSizeLimit(100000000)]
        public byte[] RenderFactSheet([FromBody] FactSheetModel factSheet)
        {
            Logger.Log(LogLevel.Debug, "Start rendering pdf for the Fact Sheet.");

            IRenderer Renderer = new FactSheetRenderer(factSheet);
            byte[] file = Renderer.Render();

            Logger.Log(LogLevel.Debug, "Finished rendering pdf for the Fact Sheet.");
            return file;
        }
    }
}
