﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDSLLabs.Public.QMSL.PDFGenerator.Renderers;
using PDSLLabs.QMSL.PDFGenerator.Models.AbsbHomepagePdf;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AbsbHomepagePdfController : ControllerBase
    {
        private ILogger<AbsbHomepagePdfController> Logger { get; }

        public AbsbHomepagePdfController(ILogger<AbsbHomepagePdfController> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Generates the PDF for an survey general report.
        /// </summary>
        /// <param name="pdf">Survey general report for which the PDF should be rendered.</param>
        /// <returns>PDF document as byte array.</returns>
        [HttpPost("RenderAbsbHomepagePdf")]
        [RequestSizeLimit(100000000)]
        public byte[] RenderSurveyPdf([FromBody] AbsbHomepagePdf pdf)
        {
            Logger.Log(LogLevel.Debug, "Start rendering pdf report.");

            IRenderer Renderer = new AbsbHomepagePdfRenderer(pdf);
            byte[] file = Renderer.Render();

            Logger.Log(LogLevel.Debug, "Finished rendering pdf report.");
            return file;
        }
    }
}
