﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDSLLabs.Public.QMSL.PDFGenerator.Renderers;
using PDSLLabs.QMSL.PDFGenerator.Models.SurveyPdf;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SurveyPdfController : ControllerBase
    {
        private ILogger<SurveyPdfController> Logger { get; }

        public SurveyPdfController(ILogger<SurveyPdfController> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Say hello!
        /// </summary>
        /// <returns>"Hello"</returns>
        [HttpGet("Hello")]
        public string Get()
        {
            return "Hello";
        }

        /// <summary>
        /// Generates the PDF for an survey general report.
        /// </summary>
        /// <param name="pdf">Survey general report for which the PDF should be rendered.</param>
        /// <returns>PDF document as byte array.</returns>
        [HttpPost("RenderSurveyPdf")]
        [RequestSizeLimit(100000000)]
        public byte[] RenderSurveyPdf([FromBody] SurveyPdf pdf)
        {
            Logger.Log(LogLevel.Debug, "Start rendering pdf report.");

            IRenderer Renderer = new SurveyPdfRenderer(pdf);
            byte[] file = Renderer.Render();

            Logger.Log(LogLevel.Debug, "Finished rendering pdf report.");
            return file;
        }
    }
}
