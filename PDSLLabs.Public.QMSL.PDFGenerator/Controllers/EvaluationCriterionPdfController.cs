﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDSLLabs.Public.QMSL.PDFGenerator.Renderers;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationCriterionPdf;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EvaluationCriterionPdfController : ControllerBase
    {
        private ILogger<EvaluationCriterionPdfController> Logger { get; }

        public EvaluationCriterionPdfController(ILogger<EvaluationCriterionPdfController> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Say hello!
        /// </summary>
        /// <returns>"Hello"</returns>
        [HttpGet("Hello")]
        public string Get()
        {
            return "Hello";
        }

        /// <summary>
        /// Generates the PDF for an evaluation criterion.
        /// </summary>
        /// <param name="pdfEvaluationCriterionTable">Evaluation criterion table for which the PDF should be rendered.</param>
        /// <returns>PDF document as byte array.</returns>
        [HttpPost("RenderEvaluationCriterionPdf")]
        [RequestSizeLimit(100000000)]
        public byte[] RenderReport([FromBody] PdfEvaluationCriterionTable pdfEvaluationCriterionTable)
        {
            Logger.Log(LogLevel.Debug, "Start rendering pdf evaluation criterion table.");

            IRenderer Renderer = new EvaluationCriterionPdfRenderer(pdfEvaluationCriterionTable);
            byte[] file = Renderer.Render();

            Logger.Log(LogLevel.Debug, "Finished rendering pdf evaluation criterion table.");
            return file;
        }
    }
}