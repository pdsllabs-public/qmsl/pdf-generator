using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;

namespace PDSLLabs.Public.QMSL.PDFGenerator
{
    public class Startup
    {
        public const string API_TITLE = "PDFGenerator API";
        public const string API_VERSION = "v1";
        public const string API_ENDPOINT = "v1/swagger.json";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Service which will be used.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                // Use the default property (Pascal) casing
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowOrigin", policyOptions => policyOptions.AllowAnyOrigin());
            });

            services.AddMvc();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(API_VERSION, new OpenApiInfo { Title = API_TITLE, Version = API_VERSION });

                string filePath = Path.Combine(AppContext.BaseDirectory, System.Reflection.Assembly.GetEntryAssembly().GetName().Name + ".xml");
                c.IncludeXmlComments(filePath);
                c.CustomSchemaIds(i => i.FullName);
            });
        }

        // this method gets called by the runtime
        // use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            string basePath = Environment.GetEnvironmentVariable("ASPNETCORE_BASEPATH") ?? "/";
            app.UsePathBase(basePath);

            app.UseCors(options => options.AllowAnyOrigin());

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                // the Swagger file also needs the correct basePath set here
                string swaggerUrl = API_ENDPOINT;
                c.SwaggerEndpoint(swaggerUrl, $"{API_TITLE} {API_VERSION}");
            });
        }
    }
}
