﻿using iText.Layout;
using PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.AbsbHomepagePdfDocStructures;
using PDSLLabs.QMSL.PDFGenerator.Models.AbsbHomepagePdf;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Renderers
{
    public class AbsbHomepagePdfRenderer : SurveyPdfRenderer
    {
        private int AppendixStart { get; set; }
        public AbsbHomepagePdfRenderer(AbsbHomepagePdf homepageReport) : base(homepageReport)
        {

        }

        /// <summary>
        /// Renders all document sections.
        /// </summary>
        protected override void RenderDocumentSections()
        {
            RenderCover();
            RenderImprint();
            RenderPreNote();
            RenderChapters();
            AppendixStart = Document.GetPdfDocument().GetNumberOfPages();
            RenderAppendix();
            PageBeforeTOC = Document.GetPdfDocument().GetNumberOfPages() - 1;
            RenderTableOfContents();
        }

        /// <summary>
        /// Renders the appendix.
        /// </summary>
        private void RenderAppendix()
        {
            new Appendix(((AbsbHomepagePdf)SurveyReport).Appendix, Document, Bookmarks, TableOfContents, SurveyReportOrientation)
           .Render();
        }

        /// <summary>
        /// Renders the document table of contents.
        /// </summary>
        protected override void RenderTableOfContents()
        {
            TableOfContents.Render(AppendixStart);
        }


        /// <summary>
        /// Renders the document cover.
        /// </summary>
        protected override void RenderCover()
        {
            new Cover(SurveyReport.Cover, Document, Bookmarks, TableOfContents, SurveyReportOrientation)
            .Render();
        }
    }
}
