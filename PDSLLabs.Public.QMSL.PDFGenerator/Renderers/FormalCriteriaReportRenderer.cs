﻿using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FormalCriteriaReport;
using System.IO;
using Footer = PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FormalCriteriaReport.Footer;
using FormalCriteriaModel = PDSLLabs.QMSL.PDFGenerator.Models.FormalCriteriaReport.FormalCriteriaReport;
using Header = PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FormalCriteriaReport.Header;
using Margins = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.FormalCriteriaReport.Margins;
namespace PDSLLabs.Public.QMSL.PDFGenerator.Renderers
{
    public class FormalCriteriaReportRenderer : IRenderer
    {
        private Document Document { get; set; }
        private PdfWriter PdfWriter { get; set; }
        private MemoryStream MemoryStream { get; set; }
        private FormalCriteriaModel formalCriteriaReport { get; }
        private HeaderFooterBase headerFooter { get; set; }

        public FormalCriteriaReportRenderer(FormalCriteriaModel _formalCriteriaReport)
        {
            formalCriteriaReport = _formalCriteriaReport;

            // open PDF document in write mode
            MemoryStream = new MemoryStream();
            PdfWriter = new PdfWriter(MemoryStream);

            // initialize PDF document
            PdfDocument pdfDocument = new(PdfWriter);

            // create document to add new elements
            Document = new Document(pdfDocument, PageSize.A4);
            Document.SetMargins(Margins.TopMargin, Margins.RightMargin, Margins.BottomMargin, Margins.LeftMargin);
        }

        /// <summary>
        /// Renders all sections for the Fact Sheet generation.
        /// </summary>
        /// <returns>The generated PDF document as byte array.</returns>
        public byte[] Render()
        {
            RenderCourseInformation();
            RenderResultsFormalCriteriaCheck();
            Document.Close();

            RenderHeaderFooter();
            return headerFooter.ResultStream.ToArray();
        }

        /// <summary>
        /// Renders all chapters.
        /// </summary>
        private void RenderCourseInformation()
        {
            new CourseInformation(formalCriteriaReport.CourseFormalCriterias, Document).Render();
        }

        /// <summary>
        /// Renders the document header and footer.
        /// </summary>
        private void RenderHeaderFooter()
        {
            headerFooter = new Header(formalCriteriaReport.Header, Document, MemoryStream);
            headerFooter.Render();
            headerFooter = new Footer(formalCriteriaReport.Footer, Document, headerFooter.ResultStream);
            headerFooter.Render();
        }

        private void RenderResultsFormalCriteriaCheck()
        {
            new ResultFormalCriteriaCheck(formalCriteriaReport.ResultFormalCriteriaCheck, Document).Render();
        }
    }
}
