using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FactSheet;
using PDSLLabs.QMSL.PDFGenerator.Models.FactSheet;
using System.IO;
using System.Linq;
using FactSheetModel = PDSLLabs.QMSL.PDFGenerator.Models.FactSheet.FactSheet;
using Margins = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.FactSheet.Margins;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Renderers
{
    public class FactSheetRenderer : IRenderer
    {
        private Document Document { get; set; }
        private PdfWriter PdfWriter { get; set; }
        private MemoryStream MemoryStream { get; set; }
        private FactSheetModel FactSheet { get; }
        private HeaderFooterBase HeaderFooter { get; set; }

        public FactSheetRenderer(FactSheetModel factSheet)
        {
            FactSheet = factSheet;

            // open PDF document in write mode
            MemoryStream = new MemoryStream();
            PdfWriter = new PdfWriter(MemoryStream);

            // initialize PDF document
            PdfDocument pdfDocument = new PdfDocument(PdfWriter);

            // create document to add new elements
            Document = new Document(pdfDocument, PageSize.A4);
            Document.SetMargins(Margins.TopMargin, Margins.RightMargin, Margins.BottomMargin, Margins.LeftMargin);
        }

        /// <summary>
        /// Renders all sections for the Fact Sheet generation.
        /// </summary>
        /// <returns>The generated PDF document as byte array.</returns>
        public byte[] Render()
        {
            try
            {
                RenderCover();
                RenderChapters();
                Document.Close();

                RenderHeaderFooter();
                return HeaderFooter.ResultStream.ToArray();
            }
            finally
            {
                MemoryStream.Dispose();
                HeaderFooter.ResultStream.Dispose();
            }
        }

        /// <summary>
        /// Renders the cover section.
        /// </summary>
        private void RenderCover()
        {
            new Cover(FactSheet.Cover, Document)
            .Render();
        }

        /// <summary>
        /// Renders all chapters.
        /// </summary>
        private void RenderChapters()
        {
            Chapter chapter = new Chapter(FactSheet.NoDataAvailable, Document);
            foreach (PDFChapter pdfChapter in FactSheet.Chapters.Where(item => item != null))
            {
                chapter.UpdateChapter(pdfChapter);
                chapter.Render();
            }
        }

        /// <summary>
        /// Renders the document header and footer.
        /// </summary>
        private void RenderHeaderFooter()
        {
            HeaderFooter = new Header(FactSheet.Header, Document, MemoryStream);
            HeaderFooter.Render();
            HeaderFooter = new Footer(FactSheet.Footer, Document, HeaderFooter.ResultStream);
            HeaderFooter.Render();
        }
    }
}
