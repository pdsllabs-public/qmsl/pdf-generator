namespace PDSLLabs.Public.QMSL.PDFGenerator.Renderers
{
    internal interface IRenderer
    {
        /// <summary>
        /// Renders the entire document according to the current renderer.
        /// </summary>
        /// <returns>The generated PDF document as byte array.</returns>
        byte[] Render();
    }
}
