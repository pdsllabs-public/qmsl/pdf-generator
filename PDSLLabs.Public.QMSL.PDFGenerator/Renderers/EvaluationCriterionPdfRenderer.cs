﻿using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationCriterion;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationCriterionPdf;
using System;
using System.IO;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationCriterionPdf;
using Footer = PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationCriterion.Footer;


namespace PDSLLabs.Public.QMSL.PDFGenerator.Renderers
{
    public class EvaluationCriterionPdfRenderer : IRenderer
    {
        private Document Document { get; set; }
        private PdfEvaluationCriterionTable PdfEvaluationCriterionTable { get; set; }
        private PdfWriter PdfWriter { get; set; }
        private MemoryStream MemoryStream { get; set; }
        private PdfDocument PdfDocument { get; set; }
        private HeaderFooterBase HeaderFooter { get; set; }

        public EvaluationCriterionPdfRenderer(PdfEvaluationCriterionTable evaluationCriterionTable)
        {
            PdfEvaluationCriterionTable = evaluationCriterionTable;

            MemoryStream = new MemoryStream();
            PdfWriter = new PdfWriter(MemoryStream);
            PdfDocument = new(PdfWriter);

            // change the page mode so that the bookmarks panel is opened by default
            PdfDocument.GetCatalog().SetPageMode(PdfName.UseOutlines);

            // create document to add new elements
            Document = new Document(PdfDocument, PageSize.A4.Rotate());
            Document.SetMargins(Margins.TopMargin, Margins.RightMargin, Margins.BottomMargin, Margins.LeftMargin);
            Document.SetProperty(Property.SPLIT_CHARACTERS, new CustomSplitCharacters());
        }

        public byte[] Render()
        {
            RenderEvaluationCriterions(PdfEvaluationCriterionTable);
            Document.Close();
            RenderHeaderFooter();
            return HeaderFooter.ResultStream.ToArray();
        }

        private void RenderEvaluationCriterions(PdfEvaluationCriterionTable ect)
        {
            Paragraph title = new(new Text(ect.EvaluationName + "\n" + $"Kriterien (Stand: {DateTime.Now:dd.MM.yyyy})\n" + ect.SelectedCourse).SetFontSize(15));

            Table table = new Table(UnitValue.CreatePercentArray(new float[] { 1f }), false).UseAllAvailableWidth().SetFixedLayout().SetKeepTogether(false).SetBorder(new SolidBorder(new DeviceRgb(66, 70, 74), 1f));

#if DEBUG
            Image checkIcon = new(ImageDataFactory.Create("./Images/check-icon.png"));
            Image notIcon = new(ImageDataFactory.Create("./Images/not-icon.png"));
#else
            Image checkIcon = new(ImageDataFactory.Create("./local/output/Images/check-icon.png"));
            Image notIcon = new(ImageDataFactory.Create("./local/output/Images/not-icon.png"));
#endif
            //each case represents one part of the pdf table (0 = Pillars, 1 = QualityTargets, 2 = Criterions, 3 = EvaluatedCriterions)
            ect.PdfEvaluations.ForEach(eval =>
            {
                Paragraph commentary = Conversion.HTMLContentToCriterion(eval.Commentary);

                switch (eval.Category)
                {
                    case 0:
                        table.AddCell(new Cell().SetKeepTogether(true).SetHeight(30f).Add(new Paragraph(eval.Pillar + "  " + eval.Name).SetMarginLeft(25f)
                            .SetPaddingTop(5f).AddStyle(Styles.Category0)).AddStyle(Styles.Category0Background));
                        break;
                    case 1:
                        table.AddCell(new Cell().SetKeepTogether(true).SetHeight(30f).Add(new Paragraph(eval.Pillar + "  " + eval.Name).SetMarginLeft(25f)
                            .SetPaddingTop(5f).AddStyle(Styles.Category1)).AddStyle(Styles.Category1Background));
                        break;
                    case 2:
                        Image icon = eval.Completed ? checkIcon : notIcon;
                        //create second table to render icon and text correctly
                        Table iconTable = new Table(UnitValue.CreatePercentArray(new float[] { 0.175f, 2.825f }), false).UseAllAvailableWidth().SetFixedLayout().SetKeepTogether(false);
                        iconTable.AddCell(new Cell().SetHeight(26f).Add(icon.ScaleToFit(14, 14).SetMarginLeft(22f).SetMarginTop(6f)).AddStyle(Styles.NoBorder));
                        iconTable.AddCell(new Cell().Add(new Paragraph("   " + eval.Pillar + "  " + eval.Name).SetMarginTop(6f).AddStyle(Styles.Category2And3)).AddStyle(Styles.NoBorder));
                        table.AddCell(new Cell().SetKeepTogether(true).Add(iconTable).AddStyle(Styles.Category2And3Background));
                        break;
                    case 3:
                        if (ect.SelectedCourse == "Alle Studiengänge")
                        {
                            table.AddCell(new Cell().SetKeepTogether(false).Add(new Paragraph(eval.CourseName).SetPaddings(5f, 0, 5f, 0)
                                .SetMarginLeft(44f).AddStyle(Styles.Category3Heading)).Add(commentary.SetMarginLeft(43f)
                                .SetPaddingBottom(3f).AddStyle(Styles.Category2And3)).AddStyle(Styles.Category2And3Background));
                        }
                        else
                        {
                            table.AddCell(new Cell().SetKeepTogether(false).Add(commentary.SetMarginLeft(44f)
                                .SetPaddings(3f, 0, 3f, 0).AddStyle(Styles.Category2And3)).AddStyle(Styles.Category2And3Background));
                        }
                        break;
                }
            });
            Document.Add(title);
            Document.Add(table);
        }

        /// <summary>
        /// Renders the document header and footer.
        /// </summary>
        private void RenderHeaderFooter()
        {
            HeaderFooter = new Footer(Document, MemoryStream);
            HeaderFooter.Render();
        }
    }
}