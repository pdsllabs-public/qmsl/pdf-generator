﻿using iText.Kernel.Pdf;
using iText.Layout;
using PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.SurveyPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using Constants = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants;
using Margins = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Margins;
using Orientation = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Orientation;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Renderers
{
    public class SurveyPdfRenderer : IRenderer
    {
        protected Document Document { get; set; }
        protected PdfWriter PdfWriter { get; set; }
        protected MemoryStream MemoryStream { get; set; }
        protected int PageBeforeTOC { get; set; }
        protected Bookmarks Bookmarks { get; }
        protected TableOfContents TableOfContents { get; }
        public SurveyPdf SurveyReport { get; }
        protected Orientation SurveyReportOrientation { get; }
        protected List<int> ExcludeLogoOnPages { get; set; } = new List<int>();

        public SurveyPdfRenderer(SurveyPdf surveyReport)
        {
            SurveyReport = surveyReport;
            SurveyReportOrientation = (Orientation)Enum.Parse(typeof(Orientation), surveyReport.PageOrientation.ToString());
            MemoryStream = new MemoryStream();
            PdfWriter = new PdfWriter(MemoryStream);

            // initialize PDF document
            PdfDocument pdfDocument = new PdfDocument(PdfWriter);

            // change the page mode so that the bookmarks panel is opened by default
            pdfDocument.GetCatalog().SetPageMode(PdfName.UseOutlines);

            pdfDocument.InitializeOutlines();
            // create document to add new elements
            Document = new Document(pdfDocument, Constants.PageOrientation[SurveyReportOrientation]);
            Document.SetMargins(Margins.TopMargin, Margins.RightMargin, Margins.OrientationMarginValues[SurveyReportOrientation][OrientationMargins.BottomMargin], Margins.LeftMargin);

            Bookmarks = new Bookmarks();
            TableOfContents = new TableOfContents(Document, SurveyReportOrientation);
        }

        /// <summary>
        /// Renders all document sections and footer/header for the survey general evaluation report generation.
        /// </summary>
        /// <returns>The generated PDF document as byte array.</returns>
        public byte[] Render()
        {
            try
            {
                RenderDocumentSections();
                Document.Close();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    MoveTocToFront(memoryStream);

                    return RenderHeaderFooter(memoryStream).ToArray();
                }
            }
            finally
            {
                MemoryStream.Dispose();
            }
        }

        /// <summary>
        /// Renders all document sections.
        /// </summary>
        protected virtual void RenderDocumentSections()
        {
            RenderCover();
            RenderImprint();
            RenderPreNote();
            RenderChapters();

            PageBeforeTOC = Document.GetPdfDocument().GetNumberOfPages() - 1;
            RenderTableOfContents();
        }

        /// <summary>
        /// Renders the document header and footer.
        /// </summary>
        /// <param name="memoryStream"></param>
        /// <returns>The generated PDF document as stream.</returns>
        protected MemoryStream RenderHeaderFooter(MemoryStream memoryStream)
        {
            Header header = new Header(SurveyReport.Header, memoryStream, ExcludeLogoOnPages.ToArray());
            header.Render();
            Footer footer = new Footer(SurveyReport.Footer, SurveyReportOrientation, header.ResultStream);
            footer.Render();

            return footer.ResultStream;
        }

        /// <summary>
        /// Moves the table of contents directly after the cover.
        /// </summary>
        /// <param name="memoryStream">The PDF document to apply the changes on.</param>
        public void MoveTocToFront(MemoryStream memoryStream)
        {
            PdfReader pdfReader = new PdfReader(new MemoryStream(MemoryStream.ToArray()));
            PdfDocument sourceDocument = new PdfDocument(pdfReader);
            PdfDocument resultDocument = new PdfDocument(new PdfWriter(memoryStream));
            // One should call this method to preserve the outlines of the source pdf file, otherwise they
            // will be absent in the resultant document to which we copy pages. In this particular sample,
            // however, this line doesn't make sense, since the source pdf lacks outlines
            resultDocument.InitializeOutlines();

            List<int> pages = new List<int>
            {
                1,
                2
            };
            for (int i = PageBeforeTOC + 1; i <= PageBeforeTOC + TableOfContents.TocPages; i++)
            {
                pages.Add(i);
            }
            for (int i = 3; i <= PageBeforeTOC; i++)
            {
                pages.Add(i);
            }

            sourceDocument.CopyPagesTo(pages, resultDocument);

            resultDocument.Close();
            sourceDocument.Close();

            ExcludeLogoOnPages = ExcludeLogoOnPages.Select(page => page <= 2 ? page : page + TableOfContents.TocPages).ToList();
        }

        /// <summary>
        /// Renders the document table of contents.
        /// </summary>
        protected virtual void RenderTableOfContents()
        {
            TableOfContents.Render(Document.GetPdfDocument().GetNumberOfPages());
        }

        /// <summary>
        /// Renders the document pre note.
        /// </summary>
        protected void RenderPreNote()
        {
            ExcludeLogoOnPages.Add(Document.GetPdfDocument().GetNumberOfPages());
            new PreNoteAndSummary(SurveyReport.PreNoteAndSummary, Document, Bookmarks, TableOfContents).Render();
        }

        /// <summary>
        /// Renders the document imprint.
        /// </summary>
        protected void RenderImprint()
        {
            ExcludeLogoOnPages.Add(Document.GetPdfDocument().GetNumberOfPages());
            new Imprint(SurveyReport.Imprint, Document, Bookmarks, TableOfContents, SurveyReportOrientation).Render();
        }

        /// <summary>
        /// Renders the document cover.
        /// </summary>
        protected virtual void RenderCover()
        {
            new Cover(SurveyReport.Cover, Document, Bookmarks, TableOfContents, SurveyReportOrientation)
            .Render();
        }

        /// <summary>
        /// Renders the document chapters.
        /// </summary>
        protected void RenderChapters()
        {
            Chapter chapter = new Chapter(Document, Bookmarks, TableOfContents, SurveyReportOrientation);
            foreach (PDFChapter pdfChapter in SurveyReport.Chapters.Where(item => item != null))
            {
                chapter.PDFChapter = pdfChapter;
                ExcludeLogoOnPages.Add(Document.GetPdfDocument().GetNumberOfPages());
                chapter.Render();
            }
        }
    }
}
