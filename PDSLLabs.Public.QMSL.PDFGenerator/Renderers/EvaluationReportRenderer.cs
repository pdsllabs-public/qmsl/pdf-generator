using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using System.Collections.Generic;
using System.IO;
using EvaluationReportModel = PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport.EvaluationReport;
using Margins = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Margins;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Renderers
{
    public class EvaluationReportRenderer : IRenderer
    {
        private Document Document { get; set; }
        private PdfWriter PdfWriter { get; set; }
        private MemoryStream MemoryStream { get; set; }
        private int PageBeforeTOC { get; set; }
        private int AppendixStart { get; set; }
        private Appendix Appendix { get; set; }
        private Abbreviations Abbreviations { get; set; }
        private Bookmarks Bookmarks { get; }
        private TableOfContents TableOfContents { get; }

        private EvaluationReportModel EvaluationReport { get; }

        public EvaluationReportRenderer(EvaluationReportModel evaluationReport)
        {
            EvaluationReport = evaluationReport;

            // open PDF document in write mode
            MemoryStream = new MemoryStream();
            PdfWriter = new PdfWriter(MemoryStream);

            // initialize PDF document
            PdfDocument pdfDocument = new(PdfWriter);

            // change the page mode so that the bookmarks panel is opened by default
            pdfDocument.GetCatalog().SetPageMode(PdfName.UseOutlines);

            // create document to add new elements
            Document = new Document(pdfDocument, PageSize.A4);
            Document.SetMargins(Margins.TopMargin, Margins.RightMargin, Margins.BottomMargin, Margins.LeftMargin);
            Document.SetProperty(Property.SPLIT_CHARACTERS, new CustomSplitCharacters());

            Bookmarks = new Bookmarks();
            TableOfContents = new TableOfContents(Document);

            Appendix = new Appendix(EvaluationReport.Appendix, Document, Bookmarks, TableOfContents);
            Abbreviations = new Abbreviations(
                EvaluationReport.Abbreviations, Document, Bookmarks, TableOfContents);

        }

        /// <summary>
        /// Renders all sections for the evaluation report, reorders pages in the document and adds a header to it.
        /// </summary>
        /// <returns>The generated PDF document as byte array.</returns>
        public byte[] Render()
        {
            try
            {
                RenderSections();
                Document.Close();

                using MemoryStream memoryStream = new();
                MoveTocToFront(memoryStream);
                return memoryStream.ToArray();
            }
            finally
            {
                MemoryStream.Dispose();
            }
        }

        /// <summary>
        /// Renders all sections for the evaluation report.
        /// </summary>
        private void RenderSections()
        {
            RenderCover();
            RenderPreface();
            RenderCourseInformation();
            RenderMeasureOverview();
            RenderFormalCriteriaResult();
            RenderEvaluationResults();
            RenderAppendix();
            RenderGlossary();
            RenderAbbreviations();
            RenderTableOfContents();
        }

        /// <summary>
        /// Renders the cover section.
        /// </summary>
        private void RenderCover()
        {
            new Cover(EvaluationReport.Cover, Document, Bookmarks, TableOfContents)
            .Render();
        }

        /// <summary>
        /// Renders the preface chapter.
        /// </summary>
        private void RenderPreface()
        {
            new Preface(EvaluationReport.Preface, Document, Bookmarks, TableOfContents)
            .Render();
        }

        /// <summary>
        /// Renders the course information chapter.
        /// </summary>
        private void RenderCourseInformation()
        {
            CourseInformation courseInformation = new(EvaluationReport.CourseInformation, Document, Bookmarks, TableOfContents);
            courseInformation.Render();
        }

        /// <summary>
        /// Renders the measure overview chapter.
        /// </summary>
        private void RenderMeasureOverview()
        {
            new MeasureOverview(
                EvaluationReport.MeasureOverview, Document, Bookmarks, TableOfContents)
                .Render();
        }

        /// <summary>
        /// Renders the formal criterion result chapter.
        /// </summary>
        private void RenderFormalCriteriaResult()
        {
            new FormalCriteriaResult(
                EvaluationReport.FormalCriteriaResult, Document, Bookmarks, TableOfContents)
                .Render();
        }

        /// <summary>
        /// Renders the section regarding the assessment of criterions
        /// </summary>
        private void RenderEvaluationResults()
        {
            EvaluationResults evaluationResults = new(EvaluationReport.EvaluationResults, Document, Bookmarks, TableOfContents);
            evaluationResults.Render();

            Appendix.UpdateAppendix(evaluationResults.CustomUploads);
        }

        /// <summary>
        /// Renders the appendix section.
        /// </summary>
        private void RenderAppendix()
        {
            AppendixStart = Document.GetPdfDocument().GetNumberOfPages();
            Appendix.Render();
        }

        /// <summary>
        /// Renders the glossary chapter.
        /// </summary>
        private void RenderGlossary()
        {
            new Glossary(
                EvaluationReport.Glossary, Document, Bookmarks, TableOfContents)
                .Render();
        }

        /// <summary>
        /// Renders the abbreviations chapter.
        /// </summary>
        private void RenderAbbreviations()
        {
            Abbreviations.Render();

            PageBeforeTOC = Document.GetPdfDocument().GetNumberOfPages();
            Abbreviations.AddPageBreak();
        }

        /// <summary>
        /// Renders the table of contents section.
        /// </summary>
        private void RenderTableOfContents()
        {
            TableOfContents.Render(AppendixStart);
        }

        /// <summary>
        /// Moves the table of contents directly after the cover and adds document header.
        /// </summary>
        /// <param name="memoryStream">The PDF document to apply the changes on.</param>
        private void MoveTocToFront(MemoryStream memoryStream)
        {
            PdfReader pdfReader = new(new MemoryStream(MemoryStream.ToArray()));
            PdfDocument sourceDocument = new(pdfReader);
            PdfDocument resultDocument = new(new PdfWriter(memoryStream));

            // the first page of a PdfDocument is 1
            // exclude appendix start page as last page if there are no attachments
            int firstPage = 1;
            int lastPage = EvaluationReport.Appendix.FileAttachments.Count > 0 ? PageBeforeTOC : PageBeforeTOC - 1;

            // this method should be called to preserve the outlines of the source pdf file
            // otherwise they will be absent in the reusulting document to which pages are copied

            // initializes an outline tree of the document and sets outline mode to true:
            // the method will read the whole document and create the outline tree
            resultDocument.InitializeOutlines();

            // reorders 'cover > chapters > toc' to 'cover > toc > chapters' 
            List<int> reorderedPages = new() { firstPage };
            for (int i = PageBeforeTOC + 1; i <= PageBeforeTOC + TableOfContents.TocPages; i++)
            {
                reorderedPages.Add(i);
            }
            for (int i = firstPage + 1; i <= lastPage; i++)
            {
                reorderedPages.Add(i);
            }

            sourceDocument.CopyPagesTo(reorderedPages, resultDocument);
            sourceDocument.Close();

            // add page numbers
            AddHeader(resultDocument);
            resultDocument.Close();
        }

        /// <summary>
        /// Adds a header to the document.
        /// </summary>
        /// <param name="pdfDocument">The PDF document.</param>
        private void AddHeader(PdfDocument pdfDocument)
        {
            Document document = new(pdfDocument);
            Rectangle ps;

            int firstPage = 1;
            int n = pdfDocument.GetNumberOfPages();

            string textLeft;
            string textRight;
            string pageNumber;

            // header for cover excluded
            for (int i = firstPage + 1; i <= n; i++)
            {
                // get page by the page number
                ps = pdfDocument.GetPage(i).GetPageSizeWithRotation();

                float height = ps.GetHeight();
                float width = ps.GetWidth();

                pageNumber = i < (AppendixStart + TableOfContents.TocPages) ?
                    i.ToString() :
                    TableOfContents.ToRoman(i + 1 - AppendixStart - TableOfContents.TocPages);

                if (i % 2 == 0)
                {
                    textLeft = pageNumber;
                    textRight = "Evaluationsbericht";
                }
                else
                {
                    textLeft = "Evaluationsbericht";
                    textRight = pageNumber;
                }

                document.ShowTextAligned(
                        new Paragraph(new Text(textLeft).AddStyle(Styles.SectionText)).SetFixedLeading(0),
                        Margins.LeftMargin,
                        height - 31.1f,
                        i,
                        TextAlignment.LEFT,
                        VerticalAlignment.TOP,
                        0
                    );

                document.ShowTextAligned(
                    new Paragraph(new Text(textRight).AddStyle(Styles.SectionText)).SetFixedLeading(0),
                    width - Margins.RightMargin,
                    height - 31.1f,
                    i,
                    TextAlignment.RIGHT,
                    VerticalAlignment.TOP,
                    0
                );

                // canvas is used for drawing a reactangle with a very small height (0.5f) just to simulate a line as boundary to the header
                // directly below the 'Evaluationsbericht' expression and page number
                PdfPage pdfPage = pdfDocument.GetPage(i);
                PdfCanvas canvas = new(pdfPage.GetLastContentStream(), pdfPage.GetResources(), pdfDocument);
                canvas.Rectangle(new Rectangle(Margins.LeftMargin, height - 48.7f, width - Margins.RightMargin - Margins.LeftMargin, 0.5f))
                .Fill();
            }
        }
    }
}
