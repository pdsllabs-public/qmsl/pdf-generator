﻿using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.MeasurePdf;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.MeasurePdf;
using System.IO;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.MeasurePdf;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Renderers
{
    public class MeasurePdfRenderer : IRenderer
    {
        private Document Document { get; set; }
        private PdfMeasureTable PdfMeasureTable { get; set; }
        private PdfWriter PdfWriter { get; set; }
        private MemoryStream MemoryStream { get; set; }
        private FooterBase Footer { get; set; }

        public MeasurePdfRenderer(PdfMeasureTable measureTable)
        {
            PdfMeasureTable = measureTable;

            MemoryStream = new MemoryStream();
            PdfWriter = new PdfWriter(MemoryStream);

            // initialize PDF document
            PdfDocument pdfDocument = new(PdfWriter);

            // change the page mode so that the bookmarks panel is opened by default
            pdfDocument.GetCatalog().SetPageMode(PdfName.UseOutlines);

            // create document to add new elements
            Document = new Document(pdfDocument, PageSize.A4.Rotate());
            Document.SetMargins(Margins.TopMargin, Margins.RightMargin, Margins.BottomMargin, Margins.LeftMargin);
            Document.SetProperty(Property.SPLIT_CHARACTERS, new CustomSplitCharacters());
        }

        public byte[] Render()
        {
            RenderMeasures(PdfMeasureTable);
            Document.Close();
            RenderFooter();
            return Footer.ResultStream.ToArray();
        }

        private void RenderMeasures(PdfMeasureTable mt)
        {
            string iconPath = "";

#if DEBUG
            iconPath = "./Images/priority-high.png";
#else
            iconPath = "./local/output/Images/priority-high.png";
#endif

            Image priorityHighIcon = new(ImageDataFactory.Create(iconPath));

            Table table = new Table(UnitValue.CreatePercentArray(new float[] { 0.5f, 0.8f, 3.1f, 3.1f, 3f, 1.3f, 2.2f, 2.7f }), false).UseAllAvailableWidth().SetFixedLayout().SetKeepTogether(false);

            table.AddCell(
                new Cell(1, 10).Add(new Paragraph(new Text(mt.Paragraphs[0]).AddStyle(Styles.Title)))
                .SetBorder(Border.NO_BORDER)
            );

            table.AddCell(
                new Cell(1, 10).Add(new Paragraph(new Text(mt.Paragraphs[1]).AddStyle(Styles.Subtitle)))
                .SetBorder(Border.NO_BORDER)
            );

            table.AddCell(new Cell(1, 1).Add(priorityHighIcon.ScaleToFit(14, 14)).SetBorder(Border.NO_BORDER));
            table.AddCell(new Cell(1, 7).Add(new Paragraph(new Text(mt.Paragraphs[2]).AddStyle(Styles.Footer))).SetBorder(Border.NO_BORDER));

            mt.Measures.ForEach(measure =>
            {
                // first row
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text("").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text("Id").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text("Quelle").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text("Thema/Studiengang").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text("Zuständigkeit").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text("Frist").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text("Status").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text("Erstelldatum").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));

                // second row
                if (measure.IsAuflage == "True")
                {
                    table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(priorityHighIcon.ScaleToFit(14, 14)).SetKeepTogether(false)).SetPaddingBottom(0);
                }
                else
                {
                    table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue));
                }
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text(measure.Id).AddStyle(Styles.TableContent)).SetKeepTogether(false)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text(measure.Source).AddStyle(Styles.TableContent)).SetKeepTogether(false)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text(measure.Topic).AddStyle(Styles.TableContent)).SetKeepTogether(false)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text(measure.Responsibilities).AddStyle(Styles.TableContent)).SetKeepTogether(false)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text(measure.Deadline).AddStyle(Styles.TableContent)).SetKeepTogether(false)).SetPaddingBottom(0));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text(measure.State).AddStyle(Styles.TableContent).SetFontColor(GetColor(measure.StateColor))).SetKeepTogether(false).SetPaddingBottom(0)));
                table.AddCell(new Cell().AddStyle(Styles.BackgroundBlue).Add(new Paragraph(new Text(measure.CreationDate).AddStyle(Styles.TableContent)).SetKeepTogether(false)).SetPaddingBottom(0));

                // third row
                table.AddCell(new Cell().AddStyle(Styles.NoBorder));
                table.AddCell(new Cell(1, 3).AddStyle(Styles.NoBorder).Add(new Paragraph(new Text("Maßnahme").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));
                table.AddCell(new Cell(1, 4).AddStyle(Styles.BorderLeft).Add(new Paragraph(new Text("Kommentar").AddStyle(Styles.TableHeader)).SetMultipliedLeading(1f)).SetPaddingBottom(0));

                // fourth row
                table.AddCell(new Cell().AddStyle(Styles.NoBorder));
                table.AddCell(new Cell(1, 3).AddStyle(Styles.NoBorder).SetKeepTogether(true).Add(new Paragraph(new Text(measure.Description).AddStyle(Styles.TableContent)).SetKeepTogether(false)).SetPaddingBottom(7f));
                table.AddCell(new Cell(1, 4).AddStyle(Styles.BorderLeft).SetKeepTogether(true).Add(new Paragraph(new Text(measure.Comments).AddStyle(Styles.TableContent)).SetKeepTogether(false)).SetPaddingBottom(7f));
            });

            Document.Add(table);
        }

        /// <summary>
        /// Renders the document header and footer.
        /// </summary>
        private void RenderFooter()
        {
            Footer = new Footer(Document, MemoryStream);
            Footer.Render();
        }

        private DeviceRgb GetColor(string stateColor)
        {
            string[] colors = stateColor.Split('-');
            int r = int.Parse(colors[0]);
            int g = int.Parse(colors[1]);
            int b = int.Parse(colors[2]);
            return new DeviceRgb(r, g, b);
        }
    }
}
