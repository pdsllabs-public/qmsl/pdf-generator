﻿using iText.IO.Image;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.AbsbHomepagePdf;
using System.Collections.Generic;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf;
using Orientation = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Orientation;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Styles;


namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.AbsbHomepagePdfDocStructures
{
    public class Appendix : SurveyPdfBaseSection
    {
        public PDFAppendix PDFAppendix { get; set; }
        private float MarginTop { get; }
        public Orientation PageOrientation { get; set; }

        public Appendix(PDFAppendix pdfAppendix, Document document, Bookmarks bookmarks, TableOfContents tableOfContents, Orientation pageOrientation) : base(document, bookmarks, tableOfContents)
        {
            PDFAppendix = pdfAppendix;
            // the margin top avoids overlapping with the header logo
            MarginTop = 30f;
            PageOrientation = pageOrientation;
        }

        /// <summary>
        /// Renders the content for the appendix.
        /// </summary>
        public override void Render()
        {
            Bookmarks.AddRootOutline(MakeTitle(PDFAppendix.Title, PDFAppendix.Numerate), RootOutline);
            TocItem tocItem = new TocItem
            {
                Title = PDFAppendix.Title,
                Indent = 0,
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            //Appendix cover
            float marginTop = Margins.OrientationMarginValues[PageOrientation][OrientationMargins.TopMarginChapterTitle];
            Paragraph paragraph = new Paragraph(new Text(PDFAppendix.Numerate).AddStyle(Styles.ChapterCoverNumber)).SetTextAlignment(TextAlignment.LEFT).SetMarginTop(marginTop)
            .SetMultipliedLeading(1f).SetDestination(MakeTitle(PDFAppendix.Title, PDFAppendix.Numerate));
            paragraph.Add(NewLine);
            paragraph.Add(new Text(PDFAppendix.Title).AddStyle(Styles.ChapterCoverTitle));
            Document.Add(paragraph);

            AddPageBreak();
            //Appendix content
            Bookmarks.AddOutline(MakeTitle(PDFAppendix.Legend.Title, PDFAppendix.Legend.Number), OutlineLevel.Level1);
            tocItem = new TocItem
            {
                Title = PDFAppendix.Legend.Title,
                Indent = 1,
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            RenderLegend();
            AddPageBreak();
            RenderExplanationResult();
            AddPageBreak();
        }

        private void RenderExplanationResult()
        {
            Bookmarks.AddOutline(MakeTitle(PDFAppendix.ExplanationResult.Title, PDFAppendix.ExplanationResult.Number), OutlineLevel.Level1);
            TocItem tocItem = new TocItem
            {
                Title = PDFAppendix.ExplanationResult.Title,
                Indent = 1,
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);
            //Header
            Paragraph paragraph = new Paragraph().SetTextAlignment(TextAlignment.LEFT).SetMultipliedLeading(1.15f).SetMarginTop(MarginTop).SetDestination(MakeTitle(PDFAppendix.ExplanationResult.Title, PDFAppendix.ExplanationResult.Number));
            // Add some extra space between '<Number>.' and title
            paragraph.AddTabStops(new TabStop(26.5f, TabAlignment.LEFT));
            Text titleNumber = new Text($"{PDFAppendix.ExplanationResult.Number}.").AddStyle(Styles.Title);
            Text titleText = new Text(PDFAppendix.ExplanationResult.Title).AddStyle(Styles.Title);
            paragraph.Add(titleNumber).Add(new Tab()).Add(titleText);
            Document.Add(paragraph);

            //Table with explanation entries
            Table table = new Table(UnitValue.CreatePercentArray(new float[] { 0.5f, 1f }))
                .SetBorder(Border.NO_BORDER)
                .SetWidth(UnitValue.CreatePercentValue(100))
                    .SetFixedLayout()
                    .SetMarginTop(30f)
                    .SetMarginLeft(25f)
                    .SetMarginBottom(5f);
            foreach (KeyValuePair<string, string> entry in PDFAppendix.ExplanationResult.TableContent)
            {
                AddCell(table, entry.Key, true);
                AddCell(table, entry.Value, false);
            }
            Document.Add(table);

            //Additional notes
            paragraph = new Paragraph(new Text(PDFAppendix.ExplanationResult.TitleAdditionalNotes).AddStyle(Styles.ExplanationsHeader)).SetTextAlignment(TextAlignment.LEFT).SetMultipliedLeading(1.15f).SetMarginTop(70f);
            Document.Add(paragraph);
            int counter = 1;
            foreach (string note in PDFAppendix.ExplanationResult.AdditionalNotes)
            {
                paragraph = new Paragraph().SetTextAlignment(TextAlignment.LEFT).SetMultipliedLeading(1.15f).SetMarginTop(-5f);
                // Add some extra space between '<Number>.' and title
                paragraph.AddTabStops(new TabStop(counter < 10 ? 15f : 20f, TabAlignment.LEFT));
                Text noteNumber = new Text(counter + ".").AddStyle(Styles.Explanations);
                Text noteText = new Text(note).AddStyle(Styles.Explanations);
                paragraph.Add(noteNumber).Add(new Tab()).Add(noteText);
                Document.Add(paragraph);
                counter++;
            }
        }

        private void AddCell(Table table, string text, bool leftColumn)
        {
            Text cellContent = new Text(text);
            cellContent.AddStyle(leftColumn ? Styles.ParagraphBold : Styles.Paragraph);

            Cell cell = new Cell().Add(
                    new Paragraph(
                        cellContent
                    ).SetMultipliedLeading(0.9f)
                ).SetKeepTogether(true)
                .SetPadding(4.7f).SetPaddingBottom(20f).SetBorder(Border.NO_BORDER);

            table.AddCell(cell);
        }

        private void RenderLegend()
        {
            Paragraph paragraph = new Paragraph().SetTextAlignment(TextAlignment.LEFT).SetMultipliedLeading(1.15f).SetMarginTop(MarginTop).SetDestination(MakeTitle(PDFAppendix.Legend.Title, PDFAppendix.Legend.Number));
            // Add some extra space between '<Number>.' and title
            paragraph.AddTabStops(new TabStop(26.5f, TabAlignment.LEFT));
            Text titleNumber = new Text($"{PDFAppendix.Legend.Number}.").AddStyle(Styles.Title);
            Text titleText = new Text(PDFAppendix.Legend.Title).AddStyle(Styles.Title);
            paragraph.Add(titleNumber).Add(new Tab()).Add(titleText);
            Document.Add(paragraph);

            if (PDFAppendix.Legend.LegendImage != null)
            {
                ImageData imageData = ImageDataFactory.Create(PDFAppendix.Legend.LegendImage);
                Image image = new Image(imageData);
                image = GetScaledImage(image);

                Table table = new Table(1, false).UseAllAvailableWidth();
                Cell cell = new Cell().Add(image).SetBorder(Border.NO_BORDER).SetPadding(0);
                table.AddCell(cell);
                Document.Add(table);
            }
        }
    }
}
