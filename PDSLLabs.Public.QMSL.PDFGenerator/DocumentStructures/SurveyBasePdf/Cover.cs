﻿using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.SurveyPdf;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf;
using Orientation = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Orientation;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures
{
    public class Cover : SurveyPdfBaseSection
    {
        public PDFCover PDFCover { get; set; }
        public Orientation PageOrientation { get; set; }

        public Cover(PDFCover cover, Document document, Bookmarks bookmarks, TableOfContents tableOfContents, Orientation pageOrientation) : base(document, bookmarks, tableOfContents)
        {
            PDFCover = cover;
            PageOrientation = pageOrientation;
        }

        /// <summary>
        /// Renders the content for the cover section.
        /// </summary>
        public override void Render()
        {
            int count = 0;
            float marginTop = Margins.OrientationMarginValues[PageOrientation][OrientationMargins.TopMarginCoverTitle];
            Document.Add(new Paragraph(new Text(PDFCover.Titles[count++]).AddStyle(Styles.CoverSubsubtitle)).SetMarginTop(marginTop).SetTextAlignment(TextAlignment.LEFT));

            Document.Add(new Paragraph(new Text(PDFCover.Titles[count++]).AddStyle(Styles.CoverSubtitle)).SetMarginTop(40f).SetMultipliedLeading(1.1f).SetTextAlignment(TextAlignment.LEFT));
            marginTop = Margins.OrientationMarginValues[PageOrientation][OrientationMargins.TopMarginCover2Title];
            Document.Add(new Paragraph(new Text(PDFCover.Titles[count++]).AddStyle(Styles.CoverTitle)).SetMarginTop(marginTop).SetMultipliedLeading(1.1f).SetTextAlignment(TextAlignment.LEFT));

            AddPageBreak();
        }
    }
}
