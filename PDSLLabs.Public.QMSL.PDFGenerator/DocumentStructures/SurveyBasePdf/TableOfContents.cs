﻿using iText.Kernel.Pdf.Action;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures
{
    public class TableOfContents : BaseTableOfContents
    {
        public TocItem PreliminaryItem { get; set; }
        public Orientation PageOrientation { get; set; }

        public TableOfContents(Document document, Orientation pageOrientation) : base(document)
        {
            PageOrientation = pageOrientation;
        }

        /// <summary>
        /// Adds the table of contents to the document.
        /// </summary>
        /// <param name="document">The PDF document.</param>
        /// <param name="appendixStart">The page number for the appendix begin.</param>
        protected override void AddTOC(Document document, int appendixStart)
        {
            // the margin top avoids overlapping with the header logo
            Paragraph paragraph = new Paragraph(new Text("Inhaltsverzeichnis").AddStyle(Styles.Title)).SetMarginTop(20f).SetMarginBottom(20f);
            document.Add(paragraph);

            float width = Document.GetPdfDocument().GetDefaultPageSize().GetWidth();
            TabStop tabStop = new TabStop(width, TabAlignment.RIGHT);
            TabStop tabStopNumber = new TabStop(25, TabAlignment.LEFT);

            Text title;
            Text page;
            if (PreliminaryItem.Title != null)
            {
                title = new Text(PreliminaryItem.Title).AddStyle(Styles.TOCContent);
                page = new Text(PreliminaryItem.Page + TocPages + "").AddStyle(Styles.TOCContent);
                paragraph = new Paragraph()
                    .AddTabStops(tabStopNumber)
                    .AddTabStops(tabStop)
                    .Add(title)
                    .SetAction(PdfAction.CreateGoTo(PreliminaryItem.Title))
                    .Add(new Tab())
                    .Add(page).SetMarginTop(Margins.OrientationMarginValues[PageOrientation][OrientationMargins.MarginTableOfContentHeadline]);
                paragraph.Add(BaseSection.NewLine);
                if (PageOrientation == Orientation.Portrait)
                {
                    paragraph.Add(BaseSection.NewLine);
                }
                document.Add(paragraph);
            }

            int count = 1;
            int appendixCounter = 1;
            float tableEntryMarginTop = 10f;
            foreach (TocItem item in TocItems)
            {
                if (item.Page >= appendixStart && item.Indent == 0)
                {
                    tableEntryMarginTop = 40f;
                }

                string titleCounterString;
                if (item.Page < appendixStart)
                {

                    titleCounterString = $"{count}";
                }
                else
                {
                    if (item.Indent == 0)
                    {
                        titleCounterString = $"A";
                    }
                    else
                    {
                        titleCounterString = $"{appendixCounter}";
                        appendixCounter++;
                    }


                }
                Text titleCounter = new Text($"{titleCounterString}.");
                title = new Text(item.Title);
                page = new Text(item.Page + TocPages + "");

                // currently is indent for all tocitems 0
                titleCounter.AddStyle(Styles.TOCContent);
                title.AddStyle(Styles.TOCContent);
                page.AddStyle(Styles.TOCContent);



                paragraph = new Paragraph()
                    .AddTabStops(tabStopNumber)
                .AddTabStops(tabStop)
                .Add(titleCounter)
                .Add(new Tab())
                .Add(title)
                .Add(new Tab())
                .Add(page)
                .SetAction(PdfAction.CreateGoTo(SurveyPdfBaseSection.MakeTitle(item.Title, titleCounterString)))
                .SetMarginTop(tableEntryMarginTop);


                paragraph.SetMarginLeft(25f * item.Indent);


                tableEntryMarginTop = 10f;
                document.Add(paragraph);
                count++;
            }
        }
    }
}
