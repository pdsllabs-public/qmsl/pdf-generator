﻿using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.SurveyPdf;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using Margins = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Margins;
using Orientation = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Orientation;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures
{
    public class Imprint : SurveyPdfBaseSection
    {
        public PDFImprint PDFImprint { get; set; }
        public Orientation PageOrientation { get; set; }

        public Imprint(PDFImprint imprint, Document document, Bookmarks bookmarks, TableOfContents tableOfContents, Orientation pageOrientation) : base(document, bookmarks, tableOfContents)
        {
            PDFImprint = imprint;
            PageOrientation = pageOrientation;
        }

        /// <summary>
        /// Renders the content of the document imprint.
        /// </summary>
        public override void Render()
        {
            float marginLeft = Margins.OrientationMarginValues[PageOrientation][OrientationMargins.LeftMarginImprint];
            float marginTop = Margins.OrientationMarginValues[PageOrientation][OrientationMargins.TopMarginImprint];
            Paragraph p = new Paragraph(new Text(PDFImprint.Title).AddStyle(Styles.Small)).SetMarginTop(marginTop).SetMarginLeft(marginLeft).SetTextAlignment(TextAlignment.LEFT)
                .SetMultipliedLeading(1.1f)
                .SetMarginBottom(10f);
            Document.Add(p);

            p.GetChildren().Clear();
            p.Add(new Text(PDFImprint.Address).AddStyle(Styles.Small)).SetMarginTop(0);

            Table t = new Table(new float[] { 100f, 100f }, false).SetMarginLeft(marginLeft).SetMarginTop(3f);

            t.SetBorder(Border.NO_BORDER);
            foreach (System.Collections.Generic.KeyValuePair<string, string> line in PDFImprint.ContactTable)
            {
                Cell cLeft = new Cell();
                Cell cRight = new Cell();
                cLeft.SetBorder(Border.NO_BORDER);
                cRight.SetBorder(Border.NO_BORDER);
                cLeft.Add(new Paragraph(new Text(line.Key).AddStyle(Styles.Small)).SetMultipliedLeading(0.8f));
                cRight.Add(new Paragraph(new Text(line.Value).AddStyle(Styles.Small)).SetMultipliedLeading(0.8f));
                t.AddCell(cLeft);
                t.AddCell(cRight);
            }

            Document.Add(p);
            Document.Add(t);
            AddPageBreak();
        }
    }
}
