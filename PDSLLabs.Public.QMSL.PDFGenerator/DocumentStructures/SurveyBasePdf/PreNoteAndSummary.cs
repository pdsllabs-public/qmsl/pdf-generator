﻿using iText.IO.Image;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.SurveyPdf;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures
{
    public class PreNoteAndSummary : SurveyPdfBaseSection
    {
        public PDFPreNoteAndSummary PDFPreNoteAndSummary { get; set; }
        public PreNoteAndSummary(PDFPreNoteAndSummary preNoteAndSummary, Document document, Bookmarks bookmarks, TableOfContents tableOfContents) : base(document, bookmarks, tableOfContents)
        {
            PDFPreNoteAndSummary = preNoteAndSummary;
        }

        /// <summary>
        /// Renders the content of the document preliminary note.
        /// </summary>
        public override void Render()
        {
            Paragraph p;
            if (PDFPreNoteAndSummary.Title != null)
            {
                Bookmarks.AddRootOutline(PDFPreNoteAndSummary.Title, RootOutline);
                TocItem tocItem = new TocItem
                {
                    Title = PDFPreNoteAndSummary.Title,
                    Indent = 0,
                    Page = GetCurrentPage()
                };
                TableOfContents.PreliminaryItem = tocItem;

                p = new Paragraph().SetTextAlignment(TextAlignment.LEFT).SetMultipliedLeading(1.15f).SetMarginTop(30f).SetDestination(PDFPreNoteAndSummary.Title);
                p.Add(new Text(PDFPreNoteAndSummary.Title).AddStyle(Styles.Title));

                foreach (string para in PDFPreNoteAndSummary.Paragraphs)
                {
                    p.Add(NewLine);
                    p.Add(NewLine);
                    p.Add(new Text(para).AddStyle(Styles.Paragraph));
                }

                Document.Add(p);
                AddPageBreak();
            }
            p = new Paragraph(new Text(PDFPreNoteAndSummary.SummaryTitle).AddStyle(Styles.Title))
            .SetTextAlignment(TextAlignment.LEFT).SetMultipliedLeading(1.15f).SetMarginTop(30f);
            Document.Add(p);

            if (PDFPreNoteAndSummary.SummaryImage != null)
            {
                ImageData imageData = ImageDataFactory.Create(PDFPreNoteAndSummary.SummaryImage);
                Image image = new Image(imageData);
                Table table = new Table(1, false).UseAllAvailableWidth();
                //-8 for not overlapping the footer
                image = GetScaledImage(image, 0, RemainingTableHeight(table) - 8);


                Cell cell = new Cell().Add(image).SetBorder(Border.NO_BORDER).SetPadding(0);
                table.AddCell(cell);
                Document.Add(table);
            }

            AddPageBreak();
        }
    }
}
