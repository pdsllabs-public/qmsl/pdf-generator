﻿using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Element;
using PDSLLabs.QMSL.PDFGenerator.Models.SurveyPdf;
using System.IO;
using System.Linq;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Conversion;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures
{
    public class Header
    {
        public PDFHeader PDFHeader { get; set; }
        public MemoryStream ResultStream { get; set; }
        public MemoryStream IncomingStream { get; set; }
        private int[] ExcludedPages { get; }

        public Header(PDFHeader header, MemoryStream incomingStream, int[] excludedPages)
        {
            PDFHeader = header;
            IncomingStream = incomingStream;
            ExcludedPages = excludedPages;
        }

        /// <summary>
        /// Renders the document header.
        /// </summary>
        public void Render()
        {
            ResultStream = new MemoryStream();
            PdfDocument pdfDocument = new(new PdfReader(new MemoryStream(IncomingStream.ToArray())), new PdfWriter(ResultStream));
            pdfDocument.InitializeOutlines();
            using Document doc = new(pdfDocument);

            for (int i = 1; i <= pdfDocument.GetNumberOfPages(); i++)
            {
                if (ExcludedPages.Contains(i))
                {
                    continue;
                }

                float paddingRight = i == 1 ? 5f : 10f;
                ImageData imageData = ImageDataFactory.Create(PDFHeader.Logo);
                Image image = new(imageData);
                Rectangle pageSize = pdfDocument.GetPage(i).GetPageSize();
                image.ScaleAbsolute(MillimetersToPoints(60), MillimetersToPoints(16));

                Rectangle imageRectangle = new(
                    image.GetImageScaledWidth() + MillimetersToPoints(paddingRight),
                    image.GetImageScaledHeight() + MillimetersToPoints(8f),
                    image.GetImageScaledWidth(),
                    image.GetImageScaledHeight()
                );

                // right-top aligned
                imageRectangle.SetX(pageSize.GetWidth() - imageRectangle.GetX());
                imageRectangle.SetY(pageSize.GetHeight() - imageRectangle.GetY());

                PdfCanvas canvas = new(pdfDocument.GetPage(i).GetLastContentStream(), pdfDocument.GetPage(i).GetResources(), pdfDocument);
                canvas.AddImageFittedIntoRectangle(imageData, imageRectangle, true);
            }
        }
    }
}
