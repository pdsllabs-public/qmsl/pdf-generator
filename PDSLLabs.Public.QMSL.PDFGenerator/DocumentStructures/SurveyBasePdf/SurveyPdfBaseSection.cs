﻿using iText.Layout;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures
{
    public abstract class SurveyPdfBaseSection : BaseSection
    {
        protected TableOfContents TableOfContents { get; set; }
        public SurveyPdfBaseSection(Document document, Bookmarks bookmarks, TableOfContents tableOfContents) : base(document, bookmarks)
        {
            TableOfContents = tableOfContents;
        }

        /// <summary>
        /// Renders the content in each section.
        /// </summary>
        public abstract void Render();

        /// <summary>
        /// Makes a numbered heading.
        /// </summary>
        /// <param name="title">The heading title.</param>
        /// <param name="number">The number, the heading should be numbered with.</param>
        /// <returns></returns>
        public static string MakeTitle(string title, int number)
        {
            return number + ". " + title;
        }

        /// <summary>
        /// Makes a numbered heading.
        /// </summary>
        /// <param name="title">The heading title.</param>
        /// <param name="numerate">The numeration, the heading should be numbered with.</param>
        /// <returns></returns>
        public static string MakeTitle(string title, string numerate)
        {
            return numerate + ". " + title;
        }
    }
}
