﻿using iText.IO.Image;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.SurveyPdf;
using System.Collections.Generic;
using System.Linq;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf;
using Orientation = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Orientation;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Styles;


namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures
{
    public class Chapter : SurveyPdfBaseSection
    {
        public PDFChapter PDFChapter { get; set; }
        private int Number { get; set; }
        private float MarginTop { get; }
        public Orientation PageOrientation { get; set; }

        public Chapter(Document document, Bookmarks bookmarks, TableOfContents tableOfContents, Orientation pageOrientation) : base(document, bookmarks, tableOfContents)
        {
            Number = 1;
            // the margin top avoids overlapping with the header logo
            MarginTop = 30f;
            PageOrientation = pageOrientation;
        }

        /// <summary>
        /// Renders the content for a chapter.
        /// </summary>
        public override void Render()
        {
            Bookmarks.AddRootOutline(MakeTitle(PDFChapter.Title, Number), RootOutline);
            TocItem tocItem = new TocItem
            {
                Title = PDFChapter.Title,
                Indent = 0,
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            float marginTop = Margins.OrientationMarginValues[PageOrientation][OrientationMargins.TopMarginChapterTitle];
            Paragraph paragraph = new Paragraph(new Text(Number.ToString("00")).AddStyle(Styles.ChapterCoverNumber)).SetTextAlignment(TextAlignment.LEFT).SetMarginTop(marginTop)
            .SetMultipliedLeading(1f).SetDestination(MakeTitle(PDFChapter.Title, Number));
            paragraph.Add(NewLine);
            paragraph.Add(new Text(PDFChapter.Title).AddStyle(Styles.ChapterCoverTitle));
            Document.Add(paragraph);

            AddPageBreak();

            paragraph = new Paragraph().SetTextAlignment(TextAlignment.LEFT).SetMultipliedLeading(1.15f).SetMarginTop(MarginTop);
            // Add some extra space between '<Number>.' and title
            paragraph.AddTabStops(new TabStop(Number < 10 ? 19f : 26.5f, TabAlignment.LEFT));
            Text titleNumber = new Text(Number + ".").AddStyle(Styles.Title);
            Text titleText = new Text(PDFChapter.Title).AddStyle(Styles.Title);
            paragraph.Add(titleNumber).Add(new Tab()).Add(titleText);
            Document.Add(paragraph);

            foreach (SurveyPdfChapterContent section in PDFChapter.ReportSections)
            {
                if (section != null)
                {
                    AddSection(section, PDFChapter.ReportSections.Count);
                }
            }

            AddPageBreak();
            Number++;
        }

        /// <summary>
        /// Adds a section to the document chapter.
        /// </summary>
        /// <param name="section">The section content to be added.</param>
        /// <param name="countReportSections">Number of existing report sections in chapter</param>
        private void AddSection(SurveyPdfChapterContent section, int countReportSections)
        {
            if (section.Reports == null || section.Reports.Count == 0)
            {
                Document.Add(ParagraphFactory.CreateTwoColumnParagraph($"Darstellung {section.Chapter}.{section.Section}", section.Text, Styles.Normal, PageOrientation));
                Document.Add(new Paragraph(new Text(section.NoDataAvailable).AddStyle(Styles.Normal)));

            }
            else
            {
                if (section.Section != 1 && PageOrientation == Orientation.Landscape)
                {
                    AddPageBreak();
                }
                string sectionTitle = $"Darstellung {section.Chapter}";
                if (countReportSections > 1)
                {
                    sectionTitle += $".{section.Section}";
                }
                sectionTitle += $" {section.Text}";
                AddReports(section.Reports, sectionTitle, section.Section);

            }

        }

        /// <summary>
        /// Adds reports for the current chapter section.
        /// </summary>
        /// <param name="reports">Reports of the current chapter section.</param>
        /// <param name="title"> The tile, wich the reports refer to.</param>
        /// /// <param name="section"> The section, wich the reports refer to.</param>
        private void AddReports(List<SurveyPdfReport> reports, string title, int section)
        {
            string[] titleSplitted = title.Split(' ');


            bool firstReport = true;
            foreach (SurveyPdfReport report in reports)
            {
                if (report.Images == null)
                {
                    if (section == 1)
                    {
                        Document.Add(ParagraphFactory.CreateTwoColumnParagraph(string.Join(" ", titleSplitted.Take(2)), string.Join(" ", titleSplitted.Skip(2)), Styles.Normal, PageOrientation, 5));
                    }
                    else
                    {
                        Document.Add(ParagraphFactory.CreateTwoColumnParagraph(string.Join(" ", titleSplitted.Take(2)), string.Join(" ", titleSplitted.Skip(2)), Styles.Normal, PageOrientation));
                    }

                    Document.Add(new Paragraph(new Text(report.NoDataAvailable).AddStyle(Styles.Normal)));
                    return;
                }

                float marginTop = 0f;
                Table table = new Table(1, false).UseAllAvailableWidth().SetKeepTogether(true).SetMarginTop(marginTop);

                float imageScaleRatio = 1f;
                float pufferBottomNextImage = Margins.OrientationMarginValues[PageOrientation][OrientationMargins.PaddingBottomReportImage];

                //variable are neccessary in case of a really large report which cannot be rendered with the section headline on one page, and needs to be scaled down
                bool reallyLargeReport = false;
                float minimizeFactor = 1f;
                for (int countImages = 1; countImages <= report.Images.Count; countImages++)
                {
                    ImageData imageData = ImageDataFactory.Create(report.Images[countImages - 1]);
                    Image image = new Image(imageData);

                    while (true)
                    {
                        float height = RemainingTableHeight(table);

                        if (firstReport && countImages == 1)
                        {
                            // if marginTop already higher than 0, there is no nedd to give more space
                            float marginInParagraph = marginTop > 0 ? 0f : section == 1 ? 5f : 15f;

                            Cell cell = new Cell()
                            .SetBorder(Border.NO_BORDER)
                            .SetPadding(0)
                            .SetPaddingBottom(3.5f)
                            .SetKeepTogether(true);

                            cell.Add(ParagraphFactory.CreateTwoColumnParagraph(
                                string.Join(" ", titleSplitted.Take(2)), string.Join(" ", titleSplitted.Skip(2)), Styles.Normal, PageOrientation, marginInParagraph)
                            );
                            table.AddCell(cell);

                            height = RemainingTableHeight(table);
                        }

                        bool lastImage = report.Images.Count > 1 && countImages == report.Images.Count;
                        Image scaledImage = GetScaledImage(image, imageScaleRatio, lastImage, marginTop, reallyLargeReport);
                        imageScaleRatio = scaledImage.GetImageScaledWidth() / scaledImage.GetImageWidth();
                        float scaledImageHeight = scaledImage.GetImageScaledHeight();

                        // if image cannot be rendered on the same page anymore
                        if (scaledImageHeight + pufferBottomNextImage + (lastImage ? 5 : 0) > height && marginTop == 0)
                        {
                            // in case both headline and first page are together on the same page, add them to doc
                            if (countImages > 1)
                            {
                                Document.Add(table);
                            }

                            // render current image on the following page
                            if (!(PageOrientation == Orientation.Landscape && firstReport && countImages == 1))
                            {
                                AddPageBreak();
                                // value is based on the paragraph-margintop for the chapter title
                                marginTop = MarginTop;

                            }
                            table = new Table(1, false).UseAllAvailableWidth().SetKeepTogether(true).SetMarginTop(marginTop);
                            if (PageOrientation == Orientation.Landscape && firstReport && countImages == 1)
                            {
                                reallyLargeReport = true;
                                //if the report is so large that it cannot be rendered after first scale down, the minimize factor increases the scaling factor
                                //to make sure that the report can be rendered after a view steps
                                imageScaleRatio -= (0.05f * minimizeFactor);
                                minimizeFactor += 0.125f;
                            }
                        }
                        else
                        {
                            // give some tolerance gap, to avoid reaching the maximal table height
                            if ((scaledImageHeight > (height - 15f) && countImages == 1) ||
                                (scaledImageHeight > (height - 15f) && height == (Document.GetRenderer().GetCurrentArea().GetBBox().GetHeight() - marginTop))
                            )
                            {
                                // 15f: small tolereance gap due to the paddingbottom of the cell created below
                                scaledImage = GetScaledImage(scaledImage, marginTop, height - 15f);
                                imageScaleRatio = scaledImage.GetImageScaledWidth() / scaledImage.GetImageWidth();
                            }

                            Cell cell = new Cell()
                            .Add(scaledImage)
                            .SetBorder(Border.NO_BORDER)
                            .SetPadding(0)
                            //increase space after one complete report to the next one
                            .SetPaddingBottom(pufferBottomNextImage + (lastImage ? 5 : 0))
                            .SetKeepTogether(true);


                            // for report with more than on image, which should be rendered on page top
                            if (countImages > 1 && table.GetChildren().Count > 0 && scaledImageHeight > height)
                            {
                                cell.SetPaddingTop(marginTop);
                            }


                            table.AddCell(cell);
                            reallyLargeReport = false;
                            minimizeFactor = 1;
                            break;
                        }
                    }
                }
                Document.Add(table);
                firstReport = false;
            }
        }

        /// <summary>
        /// Resizes image.
        /// </summary>
        /// <param name="image">Image to be resized.</param>
        /// <param name="ratio">Custom image resize ratio.</param>
        /// <param name="lastImage">Indicates, whether it is last image</param>
        /// <param name="marginTop">Optional custom maximum height.</param>
        /// <param name="reallyLargeReport">Indicates, if the report is really large</param>
        /// <returns>The resized image.</returns>
        private Image GetScaledImage(Image image, float ratio, bool lastImage, float marginTop, bool reallyLargeReport)
        {
            if (!lastImage && !reallyLargeReport)
            {
                return GetScaledImage(image, marginTop);
            }

            Image scaled = GetScaledImage(image, marginTop);
            if (ratio < 1f && scaled.GetImageScaledWidth() / scaled.GetImageWidth() > ratio)
            {
                return scaled.Scale(ratio, ratio);
            }
            return scaled;
        }
    }
}
