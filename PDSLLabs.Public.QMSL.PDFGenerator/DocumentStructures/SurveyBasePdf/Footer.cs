﻿using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.QMSL.PDFGenerator.Models.SurveyPdf;
using System.IO;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using Margins = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Margins;
using Orientation = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Orientation;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.SurveyPdfDocStructures
{
    public class Footer
    {
        public PDFFooter PDFFooter { get; set; }
        public MemoryStream ResultStream { get; set; }
        public MemoryStream IncomingStream { get; set; }
        public Orientation PageOrientation { get; set; }

        public Footer(PDFFooter footer, Orientation pageOrientation, MemoryStream incomingStream)
        {
            PDFFooter = footer;
            IncomingStream = incomingStream;
            PageOrientation = pageOrientation;
        }

        /// <summary>
        /// Renders the document footer.
        /// </summary>
        public void Render()
        {
            ResultStream = new MemoryStream();
            PdfDocument pdfDocument = new PdfDocument(new PdfReader(new MemoryStream(IncomingStream.ToArray())), new PdfWriter(ResultStream));
            pdfDocument.InitializeOutlines();
            using (Document doc = new Document(pdfDocument))
            {

                for (int i = 4; i <= pdfDocument.GetNumberOfPages(); i++)
                {

                    Rectangle pageSize = pdfDocument.GetPage(i).GetPageSize();
                    Paragraph footer = new Paragraph(new Text(PDFFooter.Text + " | " + (i)).AddStyle(Styles.Footer)).SetPaddingTop(5).SetTextAlignment(TextAlignment.RIGHT);
                    footer.SetWidth(pageSize.GetWidth() - (Margins.LeftMargin + Margins.RightMargin));
                    footer.SetBorderTop(new SolidBorder(new DeviceRgb(128, 128, 128), 0.5f));

                    float y = pageSize.GetBottom() + Margins.OrientationMarginValues[PageOrientation][OrientationMargins.FooterYPosition]; ;

                    doc.ShowTextAligned(footer, Margins.LeftMargin, y, i, TextAlignment.LEFT, VerticalAlignment.BOTTOM, 0);
                }
            }
        }
    }
}
