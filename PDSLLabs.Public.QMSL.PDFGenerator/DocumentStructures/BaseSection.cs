﻿using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Layout;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures
{
    public abstract class BaseSection
    {
        protected PdfOutline RootOutline { get; }
        protected Document Document { get; set; }
        protected Bookmarks Bookmarks { get; }
        public static Text NewLine { get; } = new Text("\n");

        public BaseSection(Document document)
        {
            Document = document;
        }
        public BaseSection(Document document, Bookmarks bookmarks)
        {
            // get root outline from the PdfDocument: false indicates iText does not need to update the outlines.
            RootOutline = document.GetPdfDocument().GetOutlines(false);
            Bookmarks = bookmarks;
            Document = document;
        }

        /// <summary>
        /// Adds page break to document.
        /// </summary>
        public void AddPageBreak()
        {
            Document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
        }

        /// <summary>
        /// Gets default page size. 
        /// </summary>
        /// <returns>The default page size.</returns>
        protected PageSize GetPageSize()
        {
            return Document.GetPdfDocument().GetDefaultPageSize();
        }

        /// <summary>
        /// Gets the current page.
        /// </summary>
        /// <returns>The current page.</returns>
        protected int GetCurrentPage()
        {
            return Document.GetPdfDocument().GetNumberOfPages();
        }

        /// <summary>
        /// Resizes image to fit page size.
        /// </summary>
        /// <param name="image">Image to be resized.</param>
        /// <param name="marginTop">Optional extra space.</param>
        /// <param name="customMaxHeight">Optional custom maximum height.</param>
        /// <returns>The resized image.</returns>
        protected Image GetScaledImage(Image image, float marginTop = 0, float customMaxHeight = 0f)
        {
            PageSize pageSize = GetPageSize();
            float maxWidth = pageSize.GetWidth() - Document.GetLeftMargin() - Document.GetRightMargin();
            float maxHeight = customMaxHeight > 0 ? customMaxHeight : pageSize.GetHeight() - Document.GetTopMargin() - Document.GetBottomMargin() - marginTop;

            if (image.GetImageWidth() > maxWidth || image.GetImageHeight() > maxHeight)
            {
                return image.ScaleToFit(maxWidth, maxHeight);
            }

            return image;
        }

        /// <summary>
        /// Gets the vertical positon, from which there is still available place to write on the current page.
        /// </summary>
        /// <param name="table">Table, which the viewport is based on.</param>
        /// <returns>The current available vertical space on the page.</returns>
        protected float RemainingTableHeight(Table table)
        {
            // document area
            Rectangle documentBox = Document.GetRenderer().GetCurrentArea().GetBBox();

            // to get the effective table width as if it was drawn on a page already, one needs to create a viewport with an area equivalent to the document area, sufficient to contain 
            // the table elements for an entire page;
            // one needs to get the subtree of renderers representing the table element and all its children and then layout it in the viewport; the maximum height of the viewport layoutarea 
            // is set to the document height (except margins)
            LayoutResult result = table.CreateRendererSubTree().SetParent(Document.GetRenderer())
                .Layout(new LayoutContext(new LayoutArea(1, new Rectangle(0, 0, documentBox.GetWidth(), documentBox.GetHeight()))));

            if (result.GetOccupiedArea() == null)
            {
                return -1f;
            }

            return result.GetOccupiedArea().GetBBox().GetY();
        }
    }
}
