﻿using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.QMSL.PDFGenerator.Models.FormalCriteriaReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.FormalCriteriaReport;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FormalCriteriaReport
{
    public class CourseInformation : BaseSection
    {
        public List<CourseFormalCriteria> courseFormalCriteria { get; set; }
        public CourseInformation(List<CourseFormalCriteria> courseFormalCriteria, Document document) : base(document)
        {
            this.courseFormalCriteria = courseFormalCriteria;
        }
        public void Render()
        {
            Document.Add(new Paragraph(new Text($"Stand des Prüfberichts: {DateTime.Now:dd.MM.yyyy}")).SetTextAlignment(TextAlignment.LEFT).AddStyle(Styles.Header).SetMarginBottom(20f));
            Document.Add(new Paragraph(new Text("Prüfbericht")).AddStyle(Styles.HeadTitle));

            foreach (CourseFormalCriteria c in courseFormalCriteria)
            {
                // convert object to dictionary<string,string>
                Dictionary<string, string> course = c.GetType()
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .ToDictionary(prop => prop.Name, prop => (string)prop.GetValue(c, null));

                Document.Add(new Paragraph(new Text(c.DegreeName + " " + c.Subject)).AddStyle(Styles.Title));

                Document.Add(new Paragraph(new Text("Verantwortung und Zuständigkeiten").AddStyle(Styles.Subtitle)).SetMarginTop(10f));

                Table firstTable = new Table(UnitValue.CreatePercentArray(new float[] { 1f, 1f }))
                    .SetWidth(UnitValue.CreatePercentValue(100))
                    .SetFixedLayout()
                    .SetMarginTop(5f)
                    .SetMarginBottom(10f);

                string[] firstTableKeys = { "FacultyName", "CourseResponsible", "CooperationPartners" };
                Dictionary<string, string> firstTableHeaders = new() { { "FacultyName", "Fakultät / Fachgruppe" }, { "CourseResponsible", "Ansprechpartner*in" }, { "CooperationPartners", "Ggf. Kooperationspartner/ weitere beteiligte Fakultäten" } };

                foreach (string key in firstTableKeys)
                {
                    AddCell(firstTable, firstTableHeaders[key], true);

                    string text = string.IsNullOrEmpty(course[key]) ? "" : course[key];
                    AddCell(firstTable, text, false);
                }

                Document.Add(firstTable);

                Document.Add(new Paragraph(new Text("Angaben zum Studiengang").AddStyle(Styles.Subtitle)).SetMarginTop(10f));

                string[] secondTableKeys = { "Subject", "DegreeName", "Specializations", "StudyType", "FirstBeginOfStudy", "BeginOfStudy", "StandardPeriodOfStudy", "TotalCP", "StudyBeginnerCount" };
                Dictionary<string, string> secondTableHeaders = new() { { "Subject", "Studiengangsbezeichnung" }, { "DegreeName", "Abschlussgrad" }, { "Specializations", "Vertiefungsrichtungen" }, { "StudyType", "Studienform" }, { "FirstBeginOfStudy", "Start des Studienbetriebs zum" }, { "BeginOfStudy", "Einschreibung möglich zum" }, { "StandardPeriodOfStudy", "Regelstudienzeit" }, { "TotalCP", "Gesamtumfang (CP)" }, { "StudyBeginnerCount", "Geplante Aufnahmezahl " } };
                Table secondTable = new Table(UnitValue.CreatePercentArray(new float[] { 1f, 1f }))
                    .SetWidth(UnitValue.CreatePercentValue(100))
                    .SetFixedLayout()
                    .SetMarginTop(5f)
                    .SetMarginBottom(10f);

                foreach (string key in secondTableKeys)
                {
                    AddCell(secondTable, secondTableHeaders[key], true);

                    string text = string.IsNullOrEmpty(course[key]) ? "" : course[key];
                    if (key == "StandardPeriodOfStudy" && !string.IsNullOrEmpty(course[key]))
                    {
                        text += " Semester";
                    }
                    else if (key == "TotalCP" && !string.IsNullOrEmpty(course[key]))
                    {
                        text += " CP";
                    }
                    AddCell(secondTable, text, false);
                }

                Document.Add(secondTable);

                Document.Add(new Paragraph(new Text("Kurzprofil des Studiengangs").AddStyle(Styles.ParagraphBold)));

                if (!string.IsNullOrEmpty(course["CourseProfile"]))
                {
                    Paragraph paragraph = Utilities.Conversion.HTMLContentToParagraph(course["CourseProfile"]);

                    Document.Add(paragraph);
                }

                if (!c.Equals(courseFormalCriteria.Last()))
                {
                    AddPageBreak();
                }

            }
        }
        private void AddCell(Table table, string text, bool leftColumn)
        {
            Text cellContent = new(text);
            cellContent.AddStyle(leftColumn ? Styles.TableHeader : Styles.TableContent);

            Cell cell = new Cell().Add(
                    new Paragraph(
                        cellContent
                    ).SetMultipliedLeading(0.9f)
                ).SetKeepTogether(true)
                .SetPadding(4.7f);

            if (leftColumn)
            {
                cell.AddStyle(Styles.BackgroundBlue);
                cell.AddStyle(Styles.TableHeader);
            }
            else
            {
                cell.AddStyle(Styles.TableContent);
            }
            table.AddCell(cell);
        }
    }
}
