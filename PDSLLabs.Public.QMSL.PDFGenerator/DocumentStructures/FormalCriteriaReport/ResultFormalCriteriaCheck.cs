﻿using iText.Layout;
using iText.Layout.Element;
using System.Collections.Generic;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.FormalCriteriaReport;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FormalCriteriaReport
{
    public class ResultFormalCriteriaCheck : BaseSection
    {
        public string resultFormalCriteriaCheck { get; set; }

        public ResultFormalCriteriaCheck(string resultFormalCriteriaCheck, Document document) : base(document)
        {
            this.resultFormalCriteriaCheck = resultFormalCriteriaCheck;
        }

        public void Render()
        {
            Document.Add(new Paragraph(new Text("Ergebnis der Prüfung der formalen Kriterien (Teil 2 StudakVO)").AddStyle(Styles.ParagraphBold)));

            if (!string.IsNullOrEmpty(resultFormalCriteriaCheck))
            {
                Paragraph paragraph = Utilities.Conversion.HTMLContentToParagraph(resultFormalCriteriaCheck);

                Document.Add(paragraph);
            }
        }
    }
}
