﻿using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using System;
using System.IO;
using System.Linq;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Conversion;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.FormalCriteriaReport;
using Alignment = PDSLLabs.QMSL.PDFGenerator.Models.FormalCriteriaReport.Alignment;
using PDFHeaderFooter = PDSLLabs.QMSL.PDFGenerator.Models.FormalCriteriaReport.PDFHeaderFooter;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FormalCriteriaReport
{
    public class Header : HeaderFooterBase
    {
        private PDFHeaderFooter PDFHeader { get; }

        public Header(PDFHeaderFooter header, Document document, MemoryStream inputStream) : base(document, inputStream)
        {
            PDFHeader = header;
        }

        /// <summary>
        /// Initializes content for header.
        /// </summary>
        protected override void InitializeElements()
        {
            PageSize pageSize = GetPageSize();

            // add titles on the left/or right side
            if (PDFHeader.Titles != null)
            {
                if (PDFHeader.Titles.ContainsKey(Alignment.Left))
                {
                    Paragraph content = new Paragraph(new Text(string.Join(Environment.NewLine, PDFHeader.Titles[Alignment.Left])).AddStyle(Styles.Paragraph)).SetMultipliedLeading(1.2f);
                    AddItem(content, Margins.LeftMargin, pageSize.GetHeight() - PDFHeader.Indent, TextAlignment.LEFT, VerticalAlignment.TOP);
                }

                if (PDFHeader.Titles.ContainsKey(Alignment.Right))
                {
                    string text = string.Join(Environment.NewLine, PDFHeader.Titles[Alignment.Right]);
                    Paragraph content = new Paragraph(new Text(text).AddStyle(Styles.Header))
                    .SetMultipliedLeading(1.2f);

                    AddItem(content, pageSize.GetWidth() - Margins.RightMargin, pageSize.GetHeight() - PDFHeader.Indent, TextAlignment.RIGHT, VerticalAlignment.TOP);
                }
            }

            // add logo on the left/or right side
            if (PDFHeader.Logo != null)
            {

                if (PDFHeader.Logo.ContainsKey(Alignment.Right))
                {
                    AddRWTHLogo(PDFHeader.Logo[Alignment.Right]);
                }

                foreach (HeaderFooterElement element in Items.Where(item => item.ImageData != null))
                {
                    Image image = new(element.ImageData);
                    image.ScaleAbsolute(MillimetersToPoints(60 * 0.6f), MillimetersToPoints(16 * 0.6f));

                    Rectangle imageRect = new(
                        image.GetImageScaledWidth() + MillimetersToPoints(12f),
                        image.GetImageScaledHeight() + MillimetersToPoints(8.5f),
                        image.GetImageScaledWidth(),
                        image.GetImageScaledHeight()
                    );

                    if (element.TextAlignment == TextAlignment.RIGHT)
                    {
                        imageRect.SetX(pageSize.GetWidth() - imageRect.GetX());
                    }

                    if (element.VerticalAlignment == VerticalAlignment.TOP)
                    {
                        imageRect.SetY(pageSize.GetHeight() - imageRect.GetY());
                    }

                    element.ImageRectangle = imageRect;
                }
            }
        }

        /// <summary>
        /// Adds new image to header.
        /// </summary>
        /// <param name="image">The image to be added.</param>
        private void AddRWTHLogo(byte[] image)
        {
            PageSize pageSize = GetPageSize();

            Items.Add(new HeaderFooterElement
            {
                ImageData = ImageDataFactory.Create(image),
                TextAlignment = TextAlignment.RIGHT,
                VerticalAlignment = VerticalAlignment.TOP,
            });
        }
    }
}
