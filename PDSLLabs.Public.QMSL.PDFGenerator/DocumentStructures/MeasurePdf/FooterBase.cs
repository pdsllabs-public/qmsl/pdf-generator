﻿using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using System.Collections.Generic;
using System.IO;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.MeasurePdf;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.MeasurePdf
{
    public abstract class FooterBase : BaseSection
    {
        private MemoryStream InputStream { get; }

        protected class FooterElement
        {
            public Paragraph Paragraph { get; set; }
            public ImageData ImageData { get; set; }
            public Rectangle ImageRectangle { get; set; }
            public float PositionX { get; set; }
            public float PositionY { get; set; }
            public TextAlignment TextAlignment { get; set; }
            public VerticalAlignment VerticalAlignment { get; set; }
        }
        protected List<FooterElement> Items { get; set; }
        protected FooterElement PageNumber { get; set; }
        public MemoryStream ResultStream { get; private set; }

        protected abstract void InitializeElements();

        public FooterBase(Document document, MemoryStream inputStream) : base(document)
        {
            InputStream = inputStream;
            Items = new List<FooterElement>();
        }

        /// <summary>
        /// Renders the content for the document header and footer.
        /// </summary>
        public void Render()
        {
            InitializeElements();

            ResultStream = new MemoryStream();
            PdfDocument pdfDocument = new(new PdfReader(new MemoryStream(InputStream.ToArray())), new PdfWriter(ResultStream));

            using Document document = new(pdfDocument);
            int firstPage = 1;
            int totalPages = pdfDocument.GetNumberOfPages();
            Paragraph pageNumberParagraph = null;
            if (PageNumber != null)
            {
                pageNumberParagraph = PageNumber.Paragraph;
            }

            for (int page = firstPage; page <= totalPages; page++)
            {
                // get page size from the given the page number
                Rectangle ps = pdfDocument.GetPage(page).GetPageSizeWithRotation();
                float height = ps.GetHeight();
                float width = ps.GetWidth();

                if (pageNumberParagraph != null)
                {
                    PageNumber.Paragraph = new Paragraph(new Text($"Seite {page} von {totalPages}")).AddStyle(Styles.Footer).SetMultipliedLeading(1.0f);
                    RenderParagraph(document, PageNumber, page);
                }
            }
        }

        /// <summary>
        /// Adds new paragraph to header/footer.
        /// </summary>
        /// <param name="paragraph">The paragraph to be added.</param>
        /// <param name="positionX">The x-position.</param>
        /// <param name="positionY">The y-position.</param>
        /// <param name="textAlignment">The text alignment.</param>
        /// <param name="verticalAlignment">The vertical alignment.</param>
        protected void AddItem(Paragraph paragraph, float positionX, float positionY, TextAlignment textAlignment, VerticalAlignment verticalAlignment)
        {
            Items.Add(new FooterElement
            {
                Paragraph = paragraph,
                PositionX = positionX,
                PositionY = positionY,
                TextAlignment = textAlignment,
                VerticalAlignment = verticalAlignment
            });
        }

        /// <summary>
        /// Adds text to the document header or footer.
        /// </summary>
        /// <param name="document">the PDF document.</param>
        /// <param name="item">the content to be inserted.</param>
        /// <param name="page">the page to insert the text.</param>
        private void RenderParagraph(Document document, FooterElement item, int page)
        {
            document.ShowTextAligned(
                item.Paragraph,
                item.PositionX,
                item.PositionY,
                page,
                item.TextAlignment,
                item.VerticalAlignment,
                0
            );
        }
    }
}
