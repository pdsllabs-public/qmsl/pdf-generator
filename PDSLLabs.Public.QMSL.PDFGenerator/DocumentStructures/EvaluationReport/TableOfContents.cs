using iText.Kernel.Pdf.Action;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class TableOfContents : BaseTableOfContents
    {
        public TableOfContents(Document document) : base(document)
        {

        }

        /// <summary>
        /// Adds the table of contents to the document.
        /// </summary>
        /// <param name="document">The PDF document.</param>
        /// <param name="appendixStart">The page number for the beginning of appendix, glossary, abbreviations chapters.</param>
        protected override void AddTOC(Document document, int appendixStart)
        {
            Paragraph paragraph = new Paragraph(new Text("Inhaltsverzeichnis").AddStyle(Styles.SectionHeader)).SetMarginBottom(20f);
            document.Add(paragraph);

            float width = document.GetPdfDocument().GetDefaultPageSize().GetWidth();
            TabStop tabStop = new(width, TabAlignment.RIGHT, new DottedLine());  // maximal document width exclusive margin

            foreach (TocItem item in TocItems)
            {
                Text title = new(item.Title);
                Text page = item.Page < appendixStart ? new Text("\t" + (TocPages + item.Page)) : new Text("\t" + ToRoman(item.Page + 1 - appendixStart));

                title.AddStyle(Styles.SectionText);
                page.AddStyle(Styles.SectionText);

                paragraph = new Paragraph()
                    .AddTabStops(tabStop)
                    .Add(title)
                    .Add(new Tab())
                    .Add(page)
                    .SetAction(PdfAction.CreateGoTo(item.Title))
                    .SetMarginTop(-1.43f)
                    .SetMarginLeft(17f * item.Indent)
                    .SetMultipliedLeading(1.4f);

                document.Add(paragraph);
            }
        }

        /// <summary>
        /// Converts a number to roman numerals.
        /// </summary>
        /// <param name="number">Page number to convert.</param>
        /// <returns>The converted number in roman numerals.</returns>
        public static string ToRoman(int number)
        {
            if (number >= 1000)
            {
                return "m" + ToRoman(number - 1000);
            }

            if (number >= 900)
            {
                return "cm" + ToRoman(number - 900);
            }

            if (number >= 500)
            {
                return "d" + ToRoman(number - 500);
            }

            if (number >= 400)
            {
                return "cd" + ToRoman(number - 400);
            }

            if (number >= 100)
            {
                return "c" + ToRoman(number - 100);
            }

            if (number >= 90)
            {
                return "xc" + ToRoman(number - 90);
            }

            if (number >= 50)
            {
                return "l" + ToRoman(number - 50);
            }

            if (number >= 40)
            {
                return "xl" + ToRoman(number - 40);
            }

            if (number >= 10)
            {
                return "x" + ToRoman(number - 10);
            }

            if (number >= 9)
            {
                return "ix" + ToRoman(number - 9);
            }

            if (number >= 5)
            {
                return "v" + ToRoman(number - 5);
            }

            if (number >= 4)
            {
                return "iv" + ToRoman(number - 4);
            }

            if (number >= 1)
            {
                return "i" + ToRoman(number - 1);
            }

            // if number < 1
            return string.Empty;
        }
    }
}
