using iText.Layout;
using iText.Layout.Element;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System.Linq;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class Preface : EvaluationReportBaseSection
    {
        private PDFPreface PdfPreface { get; }

        public Preface(PDFPreface preface, Document document, Bookmarks bookmarks, TableOfContents tableOfContents)
            : base(document, bookmarks, tableOfContents)
        {
            PdfPreface = preface;
        }

        /// <summary>
        /// Renders the content for the preface section.
        /// </summary>
        public override void Render()
        {
            string[] sections = PdfPreface.Sections.Keys.ToArray();
            int sectionCount = 0;

            Bookmarks.AddRootOutline(sections[sectionCount], RootOutline);

            TocItem tocItem = new()
            {
                Title = sections[sectionCount],
                Indent = PdfPreface.Sections[sections[sectionCount]],
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            Paragraph p = new Paragraph(new Text(sections[sectionCount]).AddStyle(Styles.SectionHeader)).SetDestination(sections[sectionCount++]);
            Document.Add(p);

            //add html paragraph
            Document.Add(Conversion.HTMLContentToParagraph(PdfPreface.Paragraph, true));

            AddPageBreak();
        }
    }
}
