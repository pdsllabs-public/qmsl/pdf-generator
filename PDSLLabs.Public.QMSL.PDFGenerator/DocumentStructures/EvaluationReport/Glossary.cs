﻿using iText.Layout;
using iText.Layout.Element;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System.Linq;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class Glossary : EvaluationReportBaseSection
    {
        private PDFGlossary PdfGlossary { get; }
        public Glossary(PDFGlossary glossary, Document document, Bookmarks bookmarks, TableOfContents tableOfContents) : base(document, bookmarks, tableOfContents)
        {
            PdfGlossary = glossary;
        }

        public override void Render()
        {
            string[] sections = PdfGlossary.Sections.Keys.ToArray();
            int sectionCount = 0;

            Bookmarks.AddRootOutline(sections[sectionCount], RootOutline);

            TocItem tocItem = new()
            {
                Title = sections[sectionCount],
                Indent = PdfGlossary.Sections[sections[sectionCount]],
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            Paragraph p = new Paragraph(new Text(sections[sectionCount]).AddStyle(Styles.SectionHeader)).SetDestination(sections[sectionCount++]);
            Document.Add(p);

            Paragraph paragraph = Utilities.Conversion.HTMLContentToParagraph(PdfGlossary.Paragraph);
            Document.Add(paragraph);

            AddPageBreak();
        }
    }
}
