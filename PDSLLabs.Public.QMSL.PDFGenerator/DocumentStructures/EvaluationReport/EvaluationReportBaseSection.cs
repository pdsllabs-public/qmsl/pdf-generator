using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Action;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public abstract class EvaluationReportBaseSection : BaseSection
    {
        protected TableOfContents TableOfContents { get; set; }

        public EvaluationReportBaseSection(Document document, Bookmarks bookmarks, TableOfContents tableOfContents) : base(document, bookmarks)
        {
            TableOfContents = tableOfContents;
        }

        /// <summary>
        /// Renders the content in each section.
        /// </summary>
        public abstract void Render();

        /// <summary>
        /// Adds upload files directly to the document if they are images.
        /// Uploads consisting of PDF files are added to the available custom uploads to be rendered as attachments in appendix
        /// and links are added to the document referring to them instead.
        /// If the custom uploads are neither PDF files nor image files an exception is thrown.
        /// </summary>
        /// <param name="uploads">New custom uploads to be considered.</param>
        /// <param name="customUploads">Already available custom uploads.</param>
        protected void AddCustomUploads(CustomUploads uploads, Dictionary<string, byte[]> customUploads)
        {
            if (uploads.Files == null)
            {
                if (!string.IsNullOrEmpty(uploads.Paragraph))
                {
                    Document.Add(ParagraphFactory.GetStandardParagraph(uploads.Paragraph));
                    return;
                }

                return;
            }

            bool imagesAvailable = false;
            foreach (KeyValuePair<string, byte[]> file in uploads.Files)
            {
                // only accept files in image or PDF format, otherwise an exception is thrown below

                // firstly, check if file is a valid image
                if (ImageDataFactory.IsSupportedType(file.Value))
                {
                    ImageData imageData = ImageDataFactory.Create(file.Value);
                    Image image = new Image(imageData)
                        .SetMarginTop(10f)
                        .SetHorizontalAlignment(HorizontalAlignment.CENTER)
                        .SetMarginBottom(15f);

                    AddScaledImage(image);
                    imagesAvailable = true;

                    continue;
                }

                // secondly, if data does not correspond to a PDF file, an exception is thrown
                new PdfReader(new MemoryStream(file.Value));

                if (!customUploads.ContainsKey(file.Key))
                {
                    customUploads.Add(file.Key, file.Value);
                }

                // ensure that the sentence is only written once
                if (customUploads.Count == 1)
                {
                    Document.Add(ParagraphFactory.GetStandardParagraph("Weitere Dokumente siehe Anhang:").SetMarginBottom(3f));
                }

                Document.Add(
                    ParagraphFactory.GetStandardParagraph(file.Key)
                    .AddStyle(Styles.Link)
                    .SetMarginBottom(3f)
                    .SetAction(PdfAction.CreateGoTo(file.Key))
                );
            };

            if (!imagesAvailable && !string.IsNullOrEmpty(uploads.Paragraph))
            {
                Document.Add(ParagraphFactory.GetStandardParagraph(uploads.Paragraph).SetMarginBottom(3f));
            }
        }

        /// <summary>
        /// Adds paragraph above the table.
        /// </summary>
        /// <param name="table">The table on which the paragraph is added.</param>
        /// <param name="text">The paragraphs content.</param>
        /// <param name="header">Flag indicating whether the content is a table header.</param>
        /// <param name="style">Given style for paragraph.</param>
        protected void AddParagraphWithLeadingToTable(Table table, string text, bool header, Style style = null)
        {
            Cell cell = new Cell().SetBorder(Border.NO_BORDER);
            Paragraph p;

            if (header)
            {
                cell.SetPaddingTop(7f).SetPaddingLeft(0f);
                p = ParagraphFactory.GetTableHeaderParagraph(text, style);
            }
            else
            {
                cell.SetPaddingTop(0f);
                p = ParagraphFactory.GetStandardParagraph(text);
            }
            p.SetKeepTogether(true);

            cell.Add(p);
            table.AddCell(cell);
        }

        /// <summary>
        /// Changes the page orientation.
        /// </summary>
        /// <param name="orientation">Orientation to change to.</param>
        protected void SetPageOrientation(Orientation orientation)
        {
            if (orientation == Orientation.Landscape)
            {
                Document.GetPdfDocument().SetDefaultPageSize(PageSize.A4.Rotate());
                return;
            }

            Document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
        }

        /// <summary>
        /// Adds line break to document.
        /// </summary>
        protected void AddLineBreak()
        {
            Document.Add(ParagraphFactory.GetTableHeaderParagraph(string.Empty));
        }

        /// <summary>
        /// Prevents the default trim of leading whitespaces in the string
        /// and immediately after a linebreak on iText7 by adding a NULL character ('\u0000')
        /// </summary>
        /// <param name="text">Text which leading whitespaces to be preserved.</param>
        /// <returns>Text with preserved leading whitespaces.</returns>
        protected string PreserveLeadingSpaces(string text)
        {
            return "\u0000" + Regex.Replace(text, @"\n", "\n\u0000");
        }

        /// <summary>
        /// Adds scaled image to document.
        /// </summary>
        /// <param name="image">Image to add do document.</param>
        private void AddScaledImage(Image image)
        {
            Document.Add(GetScaledImage(image));
        }
    }
}
