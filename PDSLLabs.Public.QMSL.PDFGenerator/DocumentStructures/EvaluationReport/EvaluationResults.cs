using iText.Kernel.Colors;
using iText.Layout;
using iText.Layout.Element;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System.Collections.Generic;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class EvaluationResults : EvaluationReportBaseSection
    {
        private PDFEvaluationResults Results { get; }
        public Dictionary<string, byte[]> CustomUploads { get; private set; }

        public EvaluationResults(PDFEvaluationResults results, Document document, Bookmarks bookmarks, TableOfContents tableOfContents)
        : base(document, bookmarks, tableOfContents)
        {
            Results = results;
            CustomUploads = new Dictionary<string, byte[]>();
        }

        /// <summary>
        /// Renders the content regarding the assessment of criterion evaluation
        /// </summary>
        public override void Render()
        {
            Document = Document;
            Results.Pillars.ForEach(pillar => RenderPillar(pillar));
        }

        /// <summary>
        /// Adds pillar information to document and goes through all its quality targets.
        /// </summary>
        /// <param name="pillar">Pillar to render into the document.</param>
        private void RenderPillar(Pillar pillar)
        {
            Bookmarks.AddRootOutline(pillar.Heading, RootOutline);

            TocItem tocItem = new()
            {
                Title = pillar.Heading,
                Indent = Results.Sections[pillar.Heading],
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            Paragraph p = new Paragraph(new Text(pillar.Heading).AddStyle(Styles.SectionHeader)).SetDestination(pillar.Heading);
            Document.Add(p);

            pillar.QualityTargets.ForEach(qualitytarget => RenderQualityTarget(qualitytarget));
        }

        /// <summary>
        /// Adds quality target information to document and goes through all its criteria.
        /// </summary>
        /// <param name="qualityTarget">Quality target to render into the document.</param>
        private void RenderQualityTarget(QualityTarget qualityTarget)
        {
            string section = qualityTarget.Paragraphs[0];
            string description = qualityTarget.Paragraphs[1];

            Bookmarks.AddOutline(section, OutlineLevel.Level1);

            TocItem tocItem = new()
            {
                Title = section,
                Indent = Results.Sections[section],
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            Style style = Styles.SubSectionHeader;
            style.SetFontColor(ColorConstants.WHITE);

            Paragraph p = new Paragraph(new Text(section).AddStyle(style)).SetDestination(section)
            .SetMultipliedLeading(1.2f);

            style = Styles.SectionText.SetItalic();
            style.SetFontColor(ColorConstants.WHITE);
            p.Add(NewLine).Add(new Text(description).AddStyle(style));

            Document.Add(Panel.GetPanel(p, Constants.DefaultBlue));

            qualityTarget.Criterions.ForEach(criterion => RenderCriterion(criterion));
        }

        /// <summary>
        /// Adds criterion information to document 
        /// </summary>
        /// <param name="criterion">Criterion to render into the document.</param>
        private void RenderCriterion(Criterion criterion)
        {
            new CriterionEvaluation(criterion.CriterionEvaluation, Results.Sections[criterion.CriterionEvaluation.Sections[0]], CustomUploads,
                Document, Bookmarks, TableOfContents)
            .Render();

            AddPageBreak();
        }
    }
}
