using iText.Kernel.Colors;
using iText.Kernel.Pdf.Action;
using iText.Layout;
using iText.Layout.Element;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using System.Collections.Generic;
using System.Linq;
using CritEval = PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport.CriterionEvaluation;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class CriterionEvaluation : EvaluationReportBaseSection
    {
        private CritEval CritEval { get; }
        private Dictionary<string, byte[]> CustomUploads { get; set; }

        public CriterionEvaluation(CritEval critEval, int sectionDepth, Dictionary<string, byte[]> customUploads, Document document,
            Bookmarks bookmarks, TableOfContents tableOfContents) : base(document, bookmarks, tableOfContents)
        {
            CritEval = critEval;
            CustomUploads = customUploads;
        }

        /// <summary>
        /// Renders the content specific to each criterion and iterates through its reports
        /// </summary>
        public override void Render()
        {
            string section = CritEval.Sections[0];
            string description = CritEval.Sections[1];

            Style style = Styles.SubSectionHeader;
            style.SetFontColor(ColorConstants.WHITE);

            Paragraph paragraph = new Paragraph(new Text(section).AddStyle(style))
            .SetMultipliedLeading(1.2f)
            .SetDestination(section);

            style = Styles.SectionText.SetItalic();
            style.SetFontColor(ColorConstants.WHITE);

            paragraph.Add(NewLine);
            paragraph.Add(new Text(description).AddStyle(style));

            DeviceRgb color = new(142, 186, 229);
            Document.Add(Panel.GetPanel(paragraph, color));

            style.SetFontColor(ColorConstants.BLACK);

            CritEval.EvaluatedCriterions?.ForEach(item =>
                {
                    if (!string.IsNullOrEmpty(item.Commentary))
                    {
                        if (string.IsNullOrEmpty(item.CourseName))
                        {
                            Document.Add(Conversion.HTMLContentToParagraph(item.Commentary).SetMarginTop(7f).SetMarginBottom(3f));
                        }
                        else
                        {
                            Document.Add(ParagraphFactory.GetCustomParagraph(item.CourseName, Styles.SectionTextBold, 7f, 3f, 1.15f));
                            Document.Add(Conversion.HTMLContentToParagraph(item.Commentary).SetMarginBottom(3f));
                        }
                    }
                });

            if (CritEval.Paragraphs != null)
            {
                Document.Add(ParagraphFactory.GetCustomParagraph(CritEval.Paragraphs[0], Styles.SectionTextBold, 7f, 3f, 1.15f)); //Ma�nahmen
                
                Paragraph p = ParagraphFactory.GetStandardParagraph(CritEval.Paragraphs[1]);
                Link anchor = new(CritEval.InternalLinks.First().Key, PdfAction.CreateGoTo(CritEval.InternalLinks.First().Value));
                anchor.AddStyle(Styles.Link);
                p.AddStyle(Styles.SectionText).Add("(s. ").Add(anchor).Add(").");
                Document.Add(p);
            }

            if (CritEval.CriterionEvaluationBaseData != null)
            {
                new CriterionEvaluationBaseData(CritEval.CriterionEvaluationBaseData, CustomUploads, Document, Bookmarks, TableOfContents)
                .Render();
            }

            if (CritEval.UploadedFilesForCriterion != null)
            {
                AddCustomUploads(CritEval.UploadedFilesForCriterion, CustomUploads);
            }
        }
    }
}
