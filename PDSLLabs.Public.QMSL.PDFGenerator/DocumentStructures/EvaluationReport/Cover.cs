using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Conversion;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;


namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class Cover : EvaluationReportBaseSection
    {
        private PDFCover PdfCover { get; }

        public Cover(PDFCover cover, Document document, Bookmarks bookmarks, TableOfContents tableOfContents)
        : base(document, bookmarks, tableOfContents)
        {
            PdfCover = cover;
        }

        /// <summary>
        /// Renders the content for the cover section.
        /// </summary>
        public override void Render()
        {
            int count = 0;
            ImageData imageData = ImageDataFactory.Create(PdfCover.Logo);
            Image image = new(imageData);
            PageSize pageSize = GetPageSize();

            image.ScaleAbsolute(MillimetersToPoints(60), MillimetersToPoints(16));
            image.SetFixedPosition(
                pageSize.GetRight() - image.GetImageScaledWidth() - MillimetersToPoints(5),
                pageSize.GetTop() - image.GetImageScaledHeight() - MillimetersToPoints(8)
            );

            Document.Add(image);

            Text text = new Text(PdfCover.Titles[count++]).AddStyle(Styles.CoverTitle);
            Document.Add(new Paragraph(text).SetFixedLeading(350).SetTextAlignment(TextAlignment.CENTER));

            Paragraph p = new Paragraph().SetMarginTop(-170f).SetMultipliedLeading(3.7f).SetTextAlignment(TextAlignment.CENTER);
            p.Add(new Text(PdfCover.Titles[count++] + "\n" + PdfCover.Titles[count++]).AddStyle(Styles.CoverSubtitle));

            Document.Add(p);

            p = new Paragraph().SetMarginTop(-30).SetMultipliedLeading(1.3f).SetTextAlignment(TextAlignment.CENTER);
            PdfCover.Titles.GetRange(count, PdfCover.Titles.Count - count)
                .ForEach(title =>
                {
                    p.Add(new Text(title).AddStyle(Styles.CoverText));
                    p.Add(NewLine);
                });

            Document.Add(p);

            p = new Paragraph().SetMarginTop(50).SetMultipliedLeading(1.3f).SetTextAlignment(TextAlignment.CENTER);
            p.Add(new Text(DateTime.Now.ToString("dd.MM.yyyy")).AddStyle(Styles.CoverText));

            Document.Add(p);

            AddPageBreak();
        }
    }
}
