using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Xobject;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;


namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    internal class Appendix : EvaluationReportBaseSection
    {
        private PDFAppendix PdfAppendix { get; }


        public Appendix(PDFAppendix appendix, Document document, Bookmarks bookmarks, TableOfContents tableOfContents)
        : base(document, bookmarks, tableOfContents)
        {
            PdfAppendix = appendix;
        }

        /// <summary>
        /// Includes custom uploads to attachments available.
        /// </summary>
        /// <param name="customUploads">Custom attachments to be added.</param>
        public void UpdateAppendix(Dictionary<string, byte[]> customUploads)
        {
            foreach (KeyValuePair<string, byte[]> item in customUploads)
            {
                if (!PdfAppendix.FileAttachments.ContainsKey(item.Key))
                {
                    PdfAppendix.FileAttachments.Add(item.Key, item.Value);
                }
            }
        }

        /// <summary>
        /// Renders the attachments for the appendix section.
        /// </summary>
        public override void Render()
        {
            if (PdfAppendix.FileAttachments.Count == 0 && PdfAppendix.ReportAttachments.Count == 0)
            {
                return;
            }

            Bookmarks.AddRootOutline(PdfAppendix.Heading, RootOutline);

            TocItem tocItem = new()
            {
                Title = PdfAppendix.Heading,
                Indent = 0,
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);
            Paragraph p = new Paragraph(new Text(PdfAppendix.Heading).AddStyle(Styles.SectionHeader)).SetDestination(PdfAppendix.Heading);
            Document.Add(p);

            int counter = 1;
            foreach (KeyValuePair<string, Dictionary<string, List<byte[]>>> attachment in PdfAppendix.ReportAttachments)
            {
                AddReportAttachment(attachment);

                if (counter < PdfAppendix.ReportAttachments.Count)
                {
                    AddPageBreak();
                }
                counter++;
            }

            foreach (KeyValuePair<string, byte[]> attachment in PdfAppendix.FileAttachments)
            {
                AddAttachment(attachment);
            }

            AddPageBreak();

            SetPageOrientation(Orientation.Portrait);
        }

        private void AddReportAttachment(KeyValuePair<string, Dictionary<string, List<byte[]>>> attachment)
        {
            TocItem tocItem = new()
            {
                Title = attachment.Key,
                Indent = 1,
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);
            Paragraph paragraph = new Paragraph(attachment.Key).AddStyle(Styles.SubSectionHeader).SetMarginBottom(8f).SetDestination(attachment.Key);
            Document.Add(paragraph);

            Table table;
            ImageData imageData;
            Image image;
            int counter = 1;
            foreach (KeyValuePair<string, List<byte[]>> reports in attachment.Value)
            {
                if (reports.Value == null)
                {
                    paragraph = new Paragraph(reports.Key).AddStyle(Styles.SectionText).SetMarginBottom(-4f);
                    Document.Add(paragraph);
                }
                else
                {
                    table = new Table(1, false).UseAllAvailableWidth().SetKeepTogether(true).SetMarginTop(20f);

                    paragraph = new Paragraph(reports.Key).AddStyle(Styles.SectionTextBold).SetMarginBottom(-4f);
                    Document.Add(paragraph);

                    foreach (byte[] imageB in reports.Value)
                    {

                        imageData = ImageDataFactory.Create(imageB);
                        image = new Image(imageData);

                        Cell cell = new Cell()
                        .Add(GetScaledImage(image))
                        .SetBorder(Border.NO_BORDER)
                        .SetPadding(0)
                        .SetPaddingBottom(20f);

                        table.AddCell(cell);
                    }
                    Document.Add(table);

                }

                if (attachment.Value.Count < counter)
                {
                    AddPageBreak();
                }
                counter++;
            }          
        }

        /// <summary>
        /// Adds a single attachment to document.
        /// </summary>
        /// <param name="item">The item composed of an attachment key and content.</param>
        private void AddAttachment(KeyValuePair<string, byte[]> item)
        {
            PdfReader attachment = new(new MemoryStream(item.Value));
            PdfDocument source = new(attachment);
            PdfDocument dest = Document.GetPdfDocument();

            // avoiding the exception 'PdfReader not opened with owner password' from third party encrypted PDF uploads involves
            // manipulating both 'unethicalReading' and 'encrypted' flags from PdfReader

            // In case the original author of the document defined permissions these permissions are ignored by setting 'unethicalReading' to true.
            // This is not a problem, as setting such permissions has become obsolete, since PDF became an ISO standard and
            // there is no longer a penalty for removing those permissions 
            // (refer to https://stackoverflow.com/questions/48064902/itext-7-pdfreader-is-not-opened-with-owner-password-error)
            attachment.SetUnethicalReading(true);

            // Mislead iText into thinking that the original PDF file was not encrypted
            attachment.GetType()
                .GetField("encrypted", BindingFlags.NonPublic | BindingFlags.Instance)
                .SetValue(attachment, false);

            // detect page orientation
            Rectangle attachmentPageSize = source.GetPage(1).GetPageSizeWithRotation();
            PageSize pageSize = GetPageSize();

            // if attachment orientation is landscape, but destination pdf is not
            if ((attachmentPageSize.GetHeight() < attachmentPageSize.GetWidth()) && ((int)Math.Round(attachmentPageSize.GetHeight()) < pageSize.GetHeight()))
            {
                SetPageOrientation(Orientation.Landscape);
            }

            // if attachment orientation is portrait but destination pdf is not
            else if ((attachmentPageSize.GetHeight() > attachmentPageSize.GetWidth()) && ((int)Math.Round(attachmentPageSize.GetHeight()) > pageSize.GetHeight()))
            {
                SetPageOrientation(Orientation.Portrait);
            }

            AddPageBreak();

            // table for inserting imported pages
            // true flag: Avoid memory overhead problems from big imported files
            Table table = new Table(1, true)
            .UseAllAvailableWidth()
            .SetDestination(item.Key);

            Document.Add(table);

            TocItem tocItem = new()
            {
                Title = item.Key,
                Indent = 1,
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            for (int i = 1; i <= source.GetNumberOfPages(); i++)
            {
                PdfPage page = source.GetPage(i);
                PdfFormXObject pageCopy = page.CopyAsFormXObject(dest);
                Image image = new(pageCopy);


                Cell cell = new Cell()
                .Add(GetScaledImage(image))
                .SetBorder(Border.NO_BORDER)
                .SetPadding(0)
                .SetKeepTogether(true);

                table.AddCell(cell);

                // flush content of a single page to render part of the table:
                // memory used be rendered cell objects is made available to the garbage collector for release
                table.Flush();
            }

            // once all the cells are added write the remainder of the table that was not rendered yet
            table.Complete();

            source.Close();
        }
    }
}
