using iText.Kernel.Colors;
using iText.Kernel.Pdf.Action;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System.Collections.Generic;
using System.Linq;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class CourseInformation : EvaluationReportBaseSection
    {
        private PDFCourseInformation Information { get; }

        public Dictionary<string, byte[]> CustomUploads { get; private set; }

        public CourseInformation(PDFCourseInformation information, Document document, Bookmarks bookmarks, TableOfContents tableOfContents)
        : base(document, bookmarks, tableOfContents)
        {
            Information = information;
            CustomUploads = new Dictionary<string, byte[]>();
        }

        /// <summary>
        /// Renders the tables and topics for course information.
        /// </summary>
        public override void Render()
        {
            // negative values indicate that a specific expression is a table header and should not be considered neither in the bookmark tree nor in table of contents
            string[] sections = Information.Sections.Where(section => section.Value >= 0).Select(section => section.Key).ToArray();
            string[] tableHeaders = Information.Sections.Where(section => section.Value < 0).Select(section => section.Key).ToArray();
            int sectionCount = 0;

            Bookmarks.AddRootOutline(sections[sectionCount], RootOutline);

            TocItem tocItem = new()
            {
                Title = sections[sectionCount],
                Indent = Information.Sections[sections[sectionCount]],
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            Paragraph p = new Paragraph(new Text(sections[sectionCount]).AddStyle(Styles.SectionHeader)).SetDestination(sections[sectionCount++]);
            Document.Add(p);


            // first table has no subsection title
            Table firstTable = CreateAndFillTable(Information.Tables.First().First());
            Document.Add(firstTable);

            // there is only one subsection title (and addition to TOC) for all remaining tables
            Bookmarks.AddOutline(sections[sectionCount], OutlineLevel.Level1);

            tocItem = new()
            {
                Title = sections[sectionCount],
                Indent = Information.Sections[sections[sectionCount]],
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            p = new Paragraph(new Text(sections[sectionCount]).AddStyle(Styles.SubSectionHeader)).SetDestination(sections[sectionCount++]);
            Document.Add(p);
            AddLineBreak();

            Table table;
            int tableHeaderCount = 0;

            foreach (List<List<CourseInformationTableEntry>> TablesForCourseInformation in Information.Tables.Skip(1))
            {
                foreach (List<CourseInformationTableEntry> entry in TablesForCourseInformation) // TODO ist eigtl. nur immer eine?
                {
                    table = CreateAndFillTable(entry, tableHeaders[tableHeaderCount++]);
                    Document.Add(table);
                }
            }

            foreach (KeyValuePair<string, List<CourseTopicInformation>> topic in Information.InformationForTopic)
            {
                AddPageBreak();

                Bookmarks.AddOutline(sections[sectionCount], OutlineLevel.Level1);

                tocItem = new()
                {
                    Title = sections[sectionCount],
                    Indent = Information.Sections[sections[sectionCount]],
                    Page = GetCurrentPage()
                };
                TableOfContents.AddToTOC(tocItem);

                p = new Paragraph(new Text(sections[sectionCount]).AddStyle(Styles.SubSectionHeader)).SetDestination(sections[sectionCount++]);
                Document.Add(p);

                topic.Value.ForEach(item =>
                {
                    // course name if topic information is course specific
                    if (!string.IsNullOrEmpty(item.Paragraphs[0]))
                    {
                        Document.Add(ParagraphFactory.GetCustomParagraph(item.Paragraphs[0], Styles.SectionTextBold, -0.3f, 3.3f, 1.5f));
                    }

                    if (item.UploadedFilesForTopic != null)
                    {
                        AddCustomUploads(item.UploadedFilesForTopic, CustomUploads);
                    }

                    // additional table for the current topic, if available (depends on topic)
                    if (item.TableForTopic.Count() != 0)
                    {

                        Table table = new Table(UnitValue.CreatePercentArray(new float[] { 1f, 1f, 1f }))
                            .SetWidth(UnitValue.CreatePercentValue(100))
                            .SetFixedLayout()
                            .SetMarginTop(-2f)
                            .SetMarginBottom(10f)
                            .SetKeepTogether(true);

                        //add table header row
                        TopicTableEntry headerRow = item.TableForTopic.First();
                        AddTopicTableCell(table, headerRow.firstCol, true, false, true);
                        AddTopicTableCell(table, headerRow.secondCol, true, false, true);
                        AddTopicTableCell(table, headerRow.thirdCol, false, false, true);

                        bool colorToggle = false;
                        foreach (TopicTableEntry entry in item.TableForTopic.Skip(1))
                        {
                            AddTopicTableCell(table, entry.firstCol, true, colorToggle);
                            AddTopicTableCell(table, entry.secondCol, true, colorToggle);
                            AddTopicTableCell(table, entry.thirdCol, false, colorToggle);

                            colorToggle = !colorToggle;
                        };

                        Document.Add(table);
                    }

                    // description for the current topic, if available (depends on topic)
                    if (item.Paragraphs.Count == 2)
                    {
                        Document.Add(Conversion.HTMLContentToParagraph(item.Paragraphs[1]));
                    }

                    if (item.TableForTopic.Count != 0) //additional pagebreak only for 'Studienverlaufsplan'
                    {
                        AddPageBreak();
                    }
                });
            }
        }

        /// <summary>
        /// Adds a new table body rows to table.
        /// </summary>
        /// <param name="table">The table on which a new cell is added.</param>
        /// <param name="text">The cell content.</param>
        /// <param name="rowColorToggle">Flag to toggle color between rows.</param>
        /// <param name="leftColumn">Flag to differentiate between left and right column.</param>
        /// <param name="isLink">Flag to differentiate between normal text an a clickable link</param>       
        private void AddTableBodyRows(Table table, string text, bool rowColorToggle, bool leftColumn, bool isLink = false)
        {
            Text cellContent;
            if (isLink)
            {
                cellContent = new Link(text, PdfAction.CreateURI(text));
            }
            else
            {
                cellContent = new Text(text);
            }
            Cell cell = new Cell().Add(
                new Paragraph(
                    cellContent.AddStyle(Styles.TableContent).SetFontColor(ColorConstants.BLACK)
                ).SetMultipliedLeading(1.05f)
                ).AddStyle(Styles.NoBorder)
                .SetPadding(4.7f);

            if (rowColorToggle)
            {
                cell.AddStyle(Styles.RowColorToggleBackgroundBlue);
            }
            if (leftColumn)
            {
                cellContent.AddStyle(Styles.TableContentBold);
                cell.AddStyle(Styles.BorderRight);
            }

            table.AddCell(cell);
        }

        /// <summary>
        /// Adds and fills a new table optinally creates header row that spans 2 cells.
        /// </summary>
        /// <param name="tableEntries">The list of entries to be added to the table.</param>
        /// <param name="tableHeader">The table header, if available.</param>

        private Table CreateAndFillTable(List<CourseInformationTableEntry> tableEntries, string tableHeader = null)
        {
            Table table = new Table(UnitValue.CreatePercentArray(new float[] { 0.6f, 1f }))
                   .SetWidth(UnitValue.CreatePercentValue(100))
                   .SetFixedLayout()
                   .SetMarginTop(-2f)
                   .SetMarginBottom(10f)
                   .SetKeepTogether(true);

            if (!string.IsNullOrEmpty(tableHeader))
            {
                Cell cell = new Cell(1, 2).Add(
                    new Paragraph(new Text(tableHeader)
                        .AddStyle(Styles.SectionTextBold)
                        .SetFontColor(ColorConstants.WHITE)
                    ).SetMultipliedLeading(0.9f)
                ).AddStyle(Styles.NoBorder)
                .AddStyle(Styles.TableHeaderBackground)
                .SetPadding(4.7f);
                table.AddHeaderCell(cell);
            }

            bool colorToggle = string.IsNullOrEmpty(tableHeader);
            foreach (CourseInformationTableEntry entry in tableEntries)
            {
                AddTableBodyRows(table, entry.Title, colorToggle, true);
                AddTableBodyRows(table, entry.Content ?? "", colorToggle, false, entry.IsLink);

                colorToggle = !colorToggle;
            }
            return table;
        }


        /// <summary>
        /// Adds a new cell to topic table.
        /// </summary>
        /// <param name="table">The table on which a new cell is added.</param>
        /// <param name="text">The cell content.</param>
        /// <param name="rightBorder"></param>
        /// <param name="colorToggle"></param>
        /// <param name="tableHeader"></param>
        private void AddTopicTableCell(Table table, string text, bool rightBorder, bool colorToggle, bool tableHeader = false)
        {
            Text cellContent = new(text);
            Cell cell = new Cell().Add(
                    new Paragraph(
                        cellContent.AddStyle(Styles.TableContent).SetFontColor(ColorConstants.BLACK)
                    ).SetMultipliedLeading(1.05f)
                ).AddStyle(Styles.NoBorder)
                .SetKeepTogether(true)
                .SetPadding(4.7f);

            if (tableHeader)
            {
                cellContent.AddStyle(Styles.TableContentBold).SetFontColor(ColorConstants.WHITE);
                cell.SetBackgroundColor(Constants.DefaultBlue);
            }

            if (colorToggle)
            {
                cell.AddStyle(Styles.RowColorToggleBackgroundBlue);
            }

            if (rightBorder)
            {
                cell.AddStyle(Styles.BorderRight);
            }

            table.AddCell(cell);
        }

    }
}
