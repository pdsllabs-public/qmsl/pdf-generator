﻿using iText.Layout;
using iText.Layout.Element;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System.Linq;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class FormalCriteriaResult : EvaluationReportBaseSection
    {
        private PDFFormalCriteriaResult FormalCriteria { get; }
        public FormalCriteriaResult(PDFFormalCriteriaResult formalCriteriaResult, Document document, Bookmarks bookmarks, TableOfContents tableOfContents) : base(document, bookmarks, tableOfContents)
        {
            FormalCriteria = formalCriteriaResult;
        }

        public override void Render()
        {
            string[] sections = FormalCriteria.Sections.Keys.ToArray();
            int sectionCount = 0;

            Bookmarks.AddRootOutline(sections[sectionCount], RootOutline);

            TocItem tocItem = new()
            {
                Title = sections[sectionCount],
                Indent = FormalCriteria.Sections[sections[sectionCount]],
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            Paragraph p = new Paragraph(new Text(sections[sectionCount]).AddStyle(Styles.SectionHeader)).SetDestination(sections[sectionCount++]);
            Document.Add(p);

            //add html paragraph
            Paragraph paragraph = Utilities.Conversion.HTMLContentToParagraph(FormalCriteria.Paragraph);
            Document.Add(paragraph);

            AddPageBreak();
        }
    }
}
