using iText.IO.Image;
using iText.Kernel.Pdf.Action;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System.Collections.Generic;
using Reports = PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport.CriterionEvaluationBaseData;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class CriterionEvaluationBaseData : EvaluationReportBaseSection
    {
        private Reports Reports { get; }
        private Table Table { get; set; }
        private Dictionary<string, byte[]> CustomUploads { get; set; }

        public CriterionEvaluationBaseData(Reports reports, Dictionary<string, byte[]> customUploads, Document document,
            Bookmarks bookmarks, TableOfContents tableOfContents) : base(document, bookmarks, tableOfContents)
        {
            Reports = reports;
            CustomUploads = customUploads;
        }

        /// <summary>
        /// Renders the report section for the current criterion.
        /// </summary>
        public override void Render()
        {
            if (Reports.Paragraphs?.Count > 1 || (Reports.Paragraphs?.Count == 1 && (Reports.ExternalLinks != null || Reports.InternalLinks != null || Reports.Reports != null)))
            {
                Table = new Table(1, false).UseAllAvailableWidth().SetKeepTogether(true);

                AddParagraphWithLeadingToTable(Table, Reports.Paragraphs[0], true); //'Datenmaterial'               

                AddLinks();

                AddReports();

                Document.Add(Table);
            }
        }

        /// <summary>
        /// Adds links for the report section.
        /// </summary>
        private void AddLinks()
        {
            if (Reports.ExternalLinks != null)
            {
                Link link;
                Paragraph hyperlink;
                foreach (KeyValuePair<string, string> item in Reports.ExternalLinks)
                {
                    link = new Link(item.Key, PdfAction.CreateURI(item.Value));
                    link.AddStyle(Styles.Link);

                    hyperlink = new Paragraph(link);

                    Table.AddCell(new Cell().Add(hyperlink).SetBorder(Border.NO_BORDER).SetPaddingLeft(0f).SetPaddingBottom(3f));
                }
            }


            if (Reports.InternalLinks != null)
            {
                foreach (KeyValuePair<string, string> item in Reports.InternalLinks)
                {
                    Link link = new(item.Key, PdfAction.CreateGoTo(item.Value));
                    link.AddStyle(Styles.Link);

                    Paragraph hyperlink = new(link);

                    Table.AddCell(new Cell().Add(hyperlink).SetBorder(Border.NO_BORDER).SetPaddingTop(0f).SetPaddingLeft(0f).SetPaddingBottom(3f));
                }
            }
        }

        /// <summary>
        /// Adds reports and/or paragraphs for the report section.
        /// </summary>
        private void AddReports()
        {
            if (Reports.Reports == null)
            {
                // index 0 is reserved for the headline
                for (int i = 1; i < Reports.Paragraphs.Count; i++)
                {
                    Paragraph p = ParagraphFactory.GetStandardParagraph(Reports.Paragraphs[i]);
                    Table.AddCell(new Cell().SetPaddingTop(0f).SetPaddingLeft(0f).SetBorder(Border.NO_BORDER).Add(p));
                }

                return;
            }

            AddPageBreak();

            bool first = true;
            // if order between images and text is important
            if (Reports.ReportElementOrder != null)
            {
                Table.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetPaddingBottom(9f));

                int countImages = 0;
                int countParagraphs = 1;
                Reports.ReportElementOrder.ForEach(type =>
                {
                    if (type == LayoutElement.Image)
                    {
                        AddImageToTable(Reports.Reports[countImages++], first);
                        first = false;
                    }

                    if (type == LayoutElement.Paragraph)
                    {
                        Paragraph p = ParagraphFactory.GetStandardParagraph(Reports.Paragraphs[countParagraphs++]);
                        Table.AddCell(new Cell().SetPaddingTop(0f).SetBorder(Border.NO_BORDER).Add(p));
                    }
                });

                return;
            }

            Reports.Reports.ForEach(item =>
            {
                if (item != null)
                {
                    AddImageToTable(item);
                }
            });
        }

        /// <summary>
        /// Adds a single report.
        /// </summary>
        /// <param name="images">List of images representing the report.</param>
        /// <param name="first"></param>
        private void AddImageToTable(List<byte[]> images, bool first = false)
        {
            ImageData imageData;
            Image image;
            foreach (byte[] imageB in images)
            {
                imageData = ImageDataFactory.Create(imageB);
                image = new Image(imageData);

                Cell cell = new Cell()
                .Add(GetScaledImage(image, first ? 42f : 0f)) // extra margin to fit on page if just headline is present
                .SetBorder(Border.NO_BORDER)
                .SetPadding(0)
                .SetPaddingBottom(15f);

                Table.AddCell(cell);
            }
        }
    }
}
