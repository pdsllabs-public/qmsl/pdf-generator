﻿using iText.Kernel.Colors;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.EvaluationReport;
using System.Linq;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationReport
{
    public class MeasureOverview : EvaluationReportBaseSection
    {
        private PDFMeasureOverview PdfMeasureOverview { get; }

        public MeasureOverview(PDFMeasureOverview overview, Document document, Bookmarks bookmarks, TableOfContents tableOfContents)
            : base(document, bookmarks, tableOfContents)
        {
            PdfMeasureOverview = overview;
        }

        public override void Render()
        {
            string[] sections = PdfMeasureOverview.Sections.Keys.ToArray();
            int sectionCount = 0;

            Bookmarks.AddRootOutline(sections[sectionCount], RootOutline);

            TocItem tocItem = new()
            {
                Title = sections[sectionCount],
                Indent = PdfMeasureOverview.Sections[sections[sectionCount]],
                Page = GetCurrentPage()
            };
            TableOfContents.AddToTOC(tocItem);

            Paragraph p = new Paragraph(new Text(sections[sectionCount]).AddStyle(Styles.SectionHeader)).SetDestination(sections[sectionCount++]);
            Document.Add(p);

            RenderMeasures(PdfMeasureOverview);

            AddPageBreak();
        }

        private void RenderMeasures(PDFMeasureOverview mo)
        {
            Table table = new Table(UnitValue.CreatePercentArray(new float[] { 0.8f, 3.0f, 3.8f, 1.2f, 2.7f }), false).UseAllAvailableWidth().SetFixedLayout().SetKeepTogether(true);

            AddTableHeaderOrBodyCell(table, "Id", true, false, true);
            AddTableHeaderOrBodyCell(table, "Studiengang/Kriterium", true, false, true);
            AddTableHeaderOrBodyCell(table, "Maßnahme", true, false, true);
            AddTableHeaderOrBodyCell(table, "Frist", true, false, true);
            AddTableHeaderOrBodyCell(table, "Zuständigkeit", true, false, true);

            bool colorToggle = false;

            mo.Table.ForEach(measure =>
            {

                AddTableHeaderOrBodyCell(table, measure.Id, true, colorToggle);
                AddTableHeaderOrBodyCell(table, $"{measure.Course}\n{measure.Topic}", true, colorToggle);
                AddTableHeaderOrBodyCell(table, measure.Description, true, colorToggle);
                AddTableHeaderOrBodyCell(table, measure.Deadline, true, colorToggle);
                AddTableHeaderOrBodyCell(table, measure.Responsibilities, false, colorToggle);

                colorToggle = !colorToggle;
            });

            Document.Add(table);
        }

        /// <summary>
        /// Adds a new header or body cell to a table.
        /// </summary>
        /// <param name="table">The table to which a new cell is added.</param>
        /// <param name="text">The cell content.</param>
        /// <param name="rightBorder"></param>
        /// <param name="colorToggle"></param>
        /// <param name="tableHeader"></param>       
        ///          
        private void AddTableHeaderOrBodyCell(Table table, string text, bool rightBorder, bool colorToggle, bool tableHeader = false)
        {
            Paragraph paragraph = new Paragraph(text).AddStyle(Styles.TableContent).SetMultipliedLeading(1.05f);

            Cell cell = new Cell().Add(paragraph).AddStyle(Styles.NoBorder)
                .SetKeepTogether(true)
                .SetPadding(4.7f);

            if (tableHeader)
            {
                paragraph.AddStyle(Styles.SectionTextBold).SetFontColor(ColorConstants.WHITE);
                cell.AddStyle(Styles.TableHeaderBackground);
            }

            if (colorToggle)
            {
                cell.AddStyle(Styles.RowColorToggleBackgroundBlue);
            }

            if (rightBorder)
            {
                cell.AddStyle(Styles.BorderRight);
            }

            table.AddCell(cell);
        }
    }
}
