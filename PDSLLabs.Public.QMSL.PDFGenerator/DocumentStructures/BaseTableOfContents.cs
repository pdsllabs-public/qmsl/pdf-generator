﻿using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using System.Collections.Generic;
using System.IO;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures
{
    public struct TocItem
    {
        public string Title { get; set; }
        public int Indent { get; set; }
        public int Page { get; set; }
    }
    public abstract class BaseTableOfContents
    {
        protected List<TocItem> TocItems { get; set; }
        public int TocPages { get; protected set; }
        protected Document Document { get; }

        public BaseTableOfContents(Document document)
        {
            TocItems = new List<TocItem>();
            Document = document;
        }
        /// <summary>
        /// Renders the content for the table of contents.
        /// </summary>
        /// <param name="appendixStart">The page number for the beginning of appendix.</param>
        public void Render(int appendixStart)
        {
            TocPages = NumerOfPagesForTOC(appendixStart);

            AddTOC(Document, appendixStart);
        }

        /// <summary>
        /// Determines the number of pages for rendering the table of contents.
        /// </summary>
        /// <param name="appendixStart">The page number for the beginning of appendix.</param>
        /// <returns></returns>
        private int NumerOfPagesForTOC(int appendixStart)
        {
            int pages = 0;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = new PdfWriter(memoryStream);
                PdfDocument pdfDocument = new PdfDocument(writer);
                PageSize pageSize = Document.GetPdfDocument().GetDefaultPageSize();

                Document document = new Document(pdfDocument, pageSize);
                document.SetMargins(
                    Document.GetTopMargin(),
                    Document.GetRightMargin(),
                    Document.GetBottomMargin(),
                    Document.GetLeftMargin()
                );

                AddTOC(document, appendixStart);
                pages = document.GetPdfDocument().GetNumberOfPages();
                document.Close();
            }

            return pages;
        }

        protected abstract void AddTOC(Document document, int appendixStart);

        /// <summary>
        /// Adds a new item to the table of contents item list.
        /// </summary>
        /// <param name="item">Item to add the table of contents item list.</param>
        public void AddToTOC(TocItem item)
        {
            TocItems.Add(item);
        }
    }
}
