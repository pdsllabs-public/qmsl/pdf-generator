﻿using iText.Kernel.Geom;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationCriterionPdf;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.EvaluationCriterion
{
    public class Footer : HeaderFooterBase
    {
        public Footer(Document document, System.IO.MemoryStream inputStream) : base(document, inputStream)
        {
        }

        /// <summary>
        ///  Initializes content for footer.
        /// </summary>
        protected override void InitializeElements()
        {
            PageSize pageSize = GetPageSize();

            PageNumber = new HeaderFooterElement
            {
                Paragraph = new Paragraph(),
                PositionX = pageSize.GetRight() - Margins.RightMargin,
                PositionY = Margins.PagenumberMargin,
                TextAlignment = TextAlignment.RIGHT,
                VerticalAlignment = VerticalAlignment.BOTTOM
            };
        }
    }
}
