﻿using iText.Layout;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.FactSheet;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FactSheet
{
    public class Cover : FactSheetBaseSection
    {
        private PDFCover PDFCover { get; }

        public Cover(PDFCover cover, Document document) : base(document)
        {
            PDFCover = cover;
        }

        /// <summary>
        /// Renders the content for the cover section.
        /// </summary>
        public void Render()
        {
            Document.Add(ParagraphFactory.GetCustomParagraph(PDFCover.Titles[0].Name, HeadlineLevel[PDFCover.Titles[0].Level], 60, 0, 1.1f)
            .SetTextAlignment(TextAlignment.LEFT)
            );

            Document.Add(ParagraphFactory.GetCustomParagraph(PDFCover.Titles[1].Name, HeadlineLevel[PDFCover.Titles[1].Level], 45, 0, 1.1f)
            .SetTextAlignment(TextAlignment.LEFT)
            );
        }
    }
}
