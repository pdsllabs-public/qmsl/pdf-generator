﻿using iText.Kernel.Geom;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.QMSL.PDFGenerator.Models.FactSheet;
using System;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.FactSheet;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FactSheet
{
    public class Footer : HeaderFooterBase
    {
        private PDFHeaderFooter PDFFooter { get; }

        public Footer(PDFHeaderFooter footer, Document document, System.IO.MemoryStream inputStream) : base(document, inputStream)
        {
            PDFFooter = footer;
        }

        /// <summary>
        ///  Initializes content for footer.
        /// </summary>
        protected override void InitializeElements()
        {
            PageSize pageSize = GetPageSize();

            // add titles on the left/or right side
            if (PDFFooter.Titles != null)
            {
                if (PDFFooter.Titles.ContainsKey(Alignment.Left))
                {
                    string text = string.Join(Environment.NewLine, PDFFooter.Titles[Alignment.Left]);
                    AddItem(text, Margins.LeftMargin, TextAlignment.LEFT);
                }

                if (PDFFooter.Titles.ContainsKey(Alignment.Right))
                {
                    string text = string.Join(Environment.NewLine, PDFFooter.Titles[Alignment.Right]);
                    AddItem(text, pageSize.GetRight() - Margins.RightMargin, TextAlignment.RIGHT);
                }
            }

            PageNumber = new HeaderFooterElement
            {
                Paragraph = new Paragraph(),
                PositionX = pageSize.GetWidth() / 2 - 20,
                PositionY = PDFFooter.Indent,
                TextAlignment = TextAlignment.LEFT,
                VerticalAlignment = VerticalAlignment.BOTTOM
            };
        }

        /// <summary>
        /// Adds new pararaph to footer.
        /// </summary>
        /// <param name="text">The paragrah text to be added.</param>
        /// <param name="positionX">The x-position.</param>
        /// <param name="alignment">The text alignment.</param>
        private void AddItem(string text, float positionX, TextAlignment alignment)
        {
            Paragraph content = new Paragraph(new Text(text).AddStyle(Styles.Footer))
            .SetMultipliedLeading(1.0f);

            AddItem(content, positionX, PDFFooter.Indent, alignment, VerticalAlignment.BOTTOM);
        }
    }
}
