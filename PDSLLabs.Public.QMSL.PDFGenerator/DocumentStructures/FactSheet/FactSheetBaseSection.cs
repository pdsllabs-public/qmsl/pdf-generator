﻿using iText.Layout;
using PDSLLabs.QMSL.PDFGenerator.Models.FactSheet;
using System.Collections.Generic;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.FactSheet.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FactSheet
{
    public abstract class FactSheetBaseSection : BaseSection
    {
        public FactSheetBaseSection(Document document) : base(document)
        {
        }

        protected Dictionary<Level, Style> HeadlineLevel { get; } = new Dictionary<Level, Style>
        {
            { Level.Level_0, Styles.Title},
            { Level.Level_1, Styles.Subtitle},
            { Level.Level_2, Styles.ChapterTitle},
            { Level.Level_3, Styles.SectionTitle},
            { Level.Level_4, Styles.SectionSubtitle},
            { Level.Level_5, Styles.SectionSubsubtitle},
            { Level.Paragraph, Styles.Paragraph},
            { Level.ReportHeader, Styles.ReportHeader},
            { Level.ReportSubHeader, Styles.ReportSubHeader},
        };
    }
}
