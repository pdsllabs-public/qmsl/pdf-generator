﻿using iText.IO.Image;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using PDSLLabs.Public.QMSL.PDFGenerator.Utilities;
using PDSLLabs.QMSL.PDFGenerator.Models.FactSheet;
using System.Collections.Generic;
using System.Linq;

namespace PDSLLabs.Public.QMSL.PDFGenerator.DocumentStructures.FactSheet
{
    public class Chapter : FactSheetBaseSection
    {
        private PDFChapter PDFChapter { get; set; }
        private Headline CommonMessage { get; }
        private Table Table { get; set; }
        private string[] ItalicStyleForExpressions { get; } = new string[] { "kohorte" };

        // control variables
        private int ReportSectionElementCounter { get; set; }
        private int MaxReportSectionsPerPage { get; } = 2;

        public Chapter(Headline commonMessage, Document document) : base(document)
        {
            CommonMessage = commonMessage;
        }

        /// <summary>
        /// Updates the chapter to render.
        /// </summary>
        /// <param name="chapter"></param>
        public void UpdateChapter(PDFChapter chapter)
        {
            PDFChapter = chapter;
        }

        /// <summary>
        /// Renders the content for a chapter.
        /// </summary>
        public void Render()
        {
            // add page break at the chapter begin
            AddPageBreak();

            Table = new Table(1, false)
                .UseAllAvailableWidth().SetKeepTogether(false);

            AddParagraphToTable(ParagraphFactory.GetCustomParagraph(PDFChapter.Title.Name, HeadlineLevel[PDFChapter.Title.Level], 5.0f, 0, 1.12f));

            for (int i = 0; i < PDFChapter.ReportSections.Count; i++)
            {
                ReportSectionElementCounter = 0;
                bool reportSectionRendered = false;
                while (!reportSectionRendered)
                {
                    if (i > 0 && i % MaxReportSectionsPerPage == 0)
                    {
                        MoveNextReportSectionToNextPage();
                    }

                    if (PDFChapter.ReportSections[i].Headlines != null)
                    {
                        foreach (Headline headline in PDFChapter.ReportSections[i].Headlines)
                        {
                            // give hihgher space in between report sections, if the report section is neither the first nor the current position is on the page top
                            float spacingBefore = i == 0 || IsCurrentPositionOnPageTop() || headline.Name != PDFChapter.ReportSections[i].Headlines[0].Name ? 3.0f : 15.0f;

                            bool isItalic = ItalicStyleForExpressions.Any(expression => headline.Name.ToLower().Contains(expression));

                            AddParagraphToTable(ParagraphFactory.GetCustomParagraph(headline.Name, HeadlineLevel[headline.Level], spacingBefore, 0, 1.1f, isItalic));
                        }
                    }

                    if (PDFChapter.ReportSections[i].Header != null)
                    {
                        PDFChapter.ReportSections[i].Header.ForEach(item =>
                        {
                            float spacingBefore = 9f;
                            if (item.Level == Level.ReportHeader)
                            {
                                spacingBefore = PDFChapter.ReportSections[i].Headlines == null && item.Level == Level.ReportHeader ? 18f : spacingBefore;
                                spacingBefore = IsCurrentPositionOnPageTop() ? 0f : spacingBefore;
                            }
                            else
                            {
                                spacingBefore = 7f;
                            }

                            AddParagraphToTable(
                                ParagraphFactory.GetCustomParagraph(item.Name, HeadlineLevel[item.Level], spacingBefore, 0, 1.12f)
                                .SetTextAlignment(TextAlignment.CENTER)
                            );
                        });
                    }

                    if (PDFChapter.ReportSections[i].Images != null)
                    {
                        if (!AddImagesToTable(PDFChapter.ReportSections[i].Images))
                        {
                            MoveNextReportSectionToNextPage();
                            continue;
                        }
                    }
                    else
                    {
                        // display the no data available message
                        AddParagraphToTable(ParagraphFactory.GetCustomParagraph(CommonMessage.Name, HeadlineLevel[CommonMessage.Level], 8.0f, 11.0f, 1.12f));
                    }

                    reportSectionRendered = true;
                }
            }

            Document.Add(Table);
        }

        /// <summary>
        /// Adds a single report.
        /// </summary>
        /// <param name="images">List of images representing the report.</param>
        private bool AddImagesToTable(List<byte[]> images)
        {
            ImageData imageData;
            Image image;

            for (int i = 0; i < images.Count; i++)
            {
                float currentHeight = RemainingTableHeight(Table);

                imageData = ImageDataFactory.Create(images[i]);
                image = new Image(imageData);

                Cell cell = new Cell()
                .Add(GetScaledImage(image))
                .SetBorder(Border.NO_BORDER)
                .SetMargin(0)
                .SetPadding(0)
                .SetPaddingTop(6f);

                Table.AddCell(cell);

                ReportSectionElementCounter++;
                float nextHeight = RemainingTableHeight(Table);
                if (currentHeight == nextHeight)
                {
                    if (i == 0)
                    {
                        RemoveReportSectionElementsFromTable();
                        return false;
                    }

                    ReportSectionElementCounter = 1;
                    RemoveReportSectionElementsFromTable();
                    MoveNextReportSectionToNextPage();
                    i--;
                }
            }

            return true;
        }

        /// <summary>
        /// Creates a cell with the given paragraph and adds it to the table.
        /// </summary>
        /// <param name="paragraph">input paragraph to be added.</param>
        private void AddParagraphToTable(Paragraph paragraph)
        {
            Cell cell = new Cell()
                .SetBorder(Border.NO_BORDER)
                .SetPadding(0);

            cell.Add(paragraph);
            Table.AddCell(cell);

            ReportSectionElementCounter++;
        }

        /// <summary>
        /// Writes the current table content to the document and sets a page break, 
        /// so that the next report section is written on the following page.
        /// </summary>
        private void MoveNextReportSectionToNextPage()
        {
            Document.Add(Table);
            AddPageBreak();
            Table = new Table(1, false)
            .UseAllAvailableWidth().SetKeepTogether(false);
        }

        /// <summary>
        /// Checks, if the current position is on the page top.
        /// </summary>
        /// <returns>True, if the condition is met.</returns>
        private bool IsCurrentPositionOnPageTop()
        {
            return RemainingTableHeight(Table) == Document.GetRenderer().GetCurrentArea().GetBBox().GetHeight();
        }

        /// <summary>
        /// Removes some oder all report section elements from the table, depending
        /// on the current report section element counter.
        /// </summary>
        private void RemoveReportSectionElementsFromTable()
        {
            for (int i = 0; i < ReportSectionElementCounter; i++)
            {
                Table.GetChildren().RemoveAt(Table.GetChildren().Count - 1);
            }
            ReportSectionElementCounter = 0;
        }
    }
}
