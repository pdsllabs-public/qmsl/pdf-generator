﻿using iText.IO.Font.Otf;
using iText.Layout.Splitting;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Utilities
{
    public class CustomSplitCharacters : DefaultSplitCharacters
    {
        public override bool IsSplitCharacter(GlyphLine text, int glyphPos)
        {
            // default itext split strategy: find empty space and hyphen characters
            if (base.IsSplitCharacter(text, glyphPos))
            {
                return true;
            }

            int charCode = text.Get(glyphPos).GetUnicode();

            return (charCode == '_' || charCode == '&' || charCode == '?' ||
                    charCode == '=' || charCode == '/');
        }
    }
}
