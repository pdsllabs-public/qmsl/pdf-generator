using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Navigation;
using System;
using System.Collections.Generic;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Utilities
{
    public class Bookmarks
    {
        private Dictionary<OutlineLevel, PdfOutline> Outlines { get; set; }

        public Bookmarks()
        {
            Outlines = new Dictionary<OutlineLevel, PdfOutline>();
        }

        /// <summary>
        /// Adds the top-level outline in the outline tree.
        /// </summary>
        /// <param name="title">The outline title.</param>
        /// <param name="root">The root outline object.</param>
        public void AddRootOutline(string title, PdfOutline root)
        {
            root = root.AddOutline(title);
            root.AddDestination(PdfDestination.MakeDestination(new PdfString(title)));

            UpdateCurrentOutline(OutlineLevel.Level0, root);
        }

        /// <summary>
        /// Adds a child outline to the current parent outline on the level above.
        /// </summary>
        /// <param name="title">The outline tile.</param>
        /// <param name="level">Level on which the new outline is added.</param>
        public void AddOutline(string title, OutlineLevel level)
        {
            OutlineLevel parentLevel = level - 1;

            if (level == OutlineLevel.Level0)
            {
                throw new Exception("The creation of a child outline on the Level 0 is not allowed.");
            }

            if (!Outlines.ContainsKey(parentLevel))
            {
                throw new Exception("Parent outline does not exist.");
            }

            PdfOutline child = Outlines[parentLevel].AddOutline(title);
            child.AddDestination(PdfDestination.MakeDestination(new PdfString(title)));

            UpdateCurrentOutline(level, child);
        }

        /// <summary>
        /// Set the new added outline as current for the given level.
        /// </summary>
        /// <param name="level">Level of currently added new outline.</param>
        /// <param name="current">New added outline.</param>
        private void UpdateCurrentOutline(OutlineLevel level, PdfOutline current)
        {
            if (Outlines.ContainsKey(level))
            {
                Outlines[level] = current;
                return;
            }

            Outlines.Add(level, current);
        }
    }
}
