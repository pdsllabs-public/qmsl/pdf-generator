using iText.Kernel.Colors;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Utilities
{
    public class Panel
    {
        /// <summary>
        /// Creates a single-column table.
        /// </summary>
        /// <param name="paragraph">Paragraph to add to the table.</param>
        /// <param name="color">Background color to apply to the cell.</param>
        /// <returns>The created single-column table.</returns>
        public static Table GetPanel(Paragraph paragraph, DeviceRgb color)
        {
            Table table = new Table(1, false)
            .SetMarginTop(5)
            .SetMarginBottom(0f)
            .SetWidth(UnitValue.CreatePercentValue(100f));


            Cell cell = new Cell()
            .Add(paragraph)
            .SetBackgroundColor(color)
            .SetPadding(10f)
            .SetBorder(Border.NO_BORDER);

            table.AddCell(cell);

            return table;
        }
    }
}
