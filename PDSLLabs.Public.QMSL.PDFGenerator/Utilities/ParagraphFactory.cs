using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals;
using static PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.SurveyPdf;
using Styles = PDSLLabs.Public.QMSL.PDFGenerator.Utilities.Globals.Constants.EvaluationReport.Styles;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Utilities
{
    public class ParagraphFactory
    {
        /// <summary>
        /// Creates a standard paragraph.
        /// </summary>
        /// <param name="text">The paragraph content.</param>
        /// <returns>The created paragraph.</returns>
        public static Paragraph GetStandardParagraph(string text)
        {
            return GetCustomParagraph(text, Styles.SectionText, 0, 9, 1.05f);
        }

        /// <summary>
        /// Creates a paragraph for the table header.
        /// </summary>
        /// <param name="text">The paragraph content.</param>
        /// <returns>The created paragraph.</returns>
        public static Paragraph GetTableHeaderParagraph(string text)
        {
            return GetTableHeaderParagraph(text, null);
        }

        /// <summary>
        /// Creates a paragraph for the table header.
        /// </summary>
        /// <param name="text">The paragraph content.</param>
        /// <param name="style">The paragraph style.</param>
        /// <returns>The created paragraph.</returns>
        public static Paragraph GetTableHeaderParagraph(string text, Style style)
        {
            float multiplyLeading = 1.15f;
            style ??= Styles.SectionTextBold;

            return GetCustomParagraph(text, style, 0, 3.5f, multiplyLeading);
        }

        /// <summary>
        /// Creates a paragraph with custom properties.
        /// </summary>
        /// <param name="text">The paragraph content.</param>
        /// <param name="font">The paragraph style.</param>
        /// <param name="spacingbefore">The paragraph top margin width.</param>
        /// <param name="spacingafter">The paragraph bottom margin width.</param>
        /// <param name="multipliedLeading">The paragraph leading value.</param>
        /// <param name="isItalic">The italic stle for a font if true.</param>
        /// <returns>The created paragraph.</returns>
        public static Paragraph GetCustomParagraph(string text, Style font, float spacingbefore, float spacingafter, float multipliedLeading, bool isItalic = false)
        {
            Text styledText = new(text);
            styledText.AddStyle(font);

            Paragraph paragraph = new(styledText);
            // remark: 1.5 is itext Standard
            paragraph.SetMultipliedLeading(multipliedLeading);
            paragraph.SetMarginTop(spacingbefore);
            paragraph.SetMarginBottom(spacingafter);
            paragraph.SetTextAlignment(TextAlignment.JUSTIFIED);

            return isItalic ? paragraph.SetItalic() : paragraph;
        }

        /// <summary>
        /// Creates a paragraph with content split in two columns.
        /// </summary>
        /// <param name="textLeft">The text to be rendered in the left column.</param>
        /// <param name="textRight">The text to be rendered in the right column.</param>
        /// <param name="font">The paragraph style.</param>
        /// <param name="marginTop">Optional paragraph margin top.</param>
        /// <param name="pageOrientation">Orientation of document</param>
        /// <returns>Paragraph with the given content.</returns>
        public static Paragraph CreateTwoColumnParagraph(string textLeft, string textRight, Style font, Orientation pageOrientation, float marginTop = 15f)
        {
            float firstColumnWidth = Margins.OrientationMarginValues[pageOrientation][OrientationMargins.SectionHeadingFirstColumnPercentage];
            Paragraph paragraph = new Paragraph().SetTextAlignment(TextAlignment.LEFT).SetMultipliedLeading(1f).SetMarginTop(marginTop);
            Table table = new Table(new UnitValue[] { new(UnitValue.PERCENT, firstColumnWidth), new(UnitValue.PERCENT, 100f - firstColumnWidth) }).UseAllAvailableWidth();

            table.SetBorder(Border.NO_BORDER);
            Cell cLeft = new();
            Cell cRight = new();
            cLeft.SetBorder(Border.NO_BORDER);
            cRight.SetBorder(Border.NO_BORDER);
            if (!string.IsNullOrEmpty(textLeft))
            {
                cLeft.Add(new Paragraph(new Text(textLeft)).AddStyle(font).SetMultipliedLeading(1f));
            }
            if (!string.IsNullOrEmpty(textRight))
            {
                cRight.Add(new Paragraph(new Text(textRight)).AddStyle(font).SetMultipliedLeading(1f));
            }
            table.AddCell(cLeft);
            table.AddCell(cRight);
            paragraph.Add(table);

            return paragraph;
        }
        public static Paragraph GetEmptyParagraph(float spacingbefore, float spacingafter, float multipliedLeading)
        {
            Paragraph paragraph = new();
            paragraph.SetMarginTop(spacingbefore);
            paragraph.SetMarginBottom(spacingafter);
            paragraph.SetMultipliedLeading(multipliedLeading);

            return paragraph;
        }
    }
}
