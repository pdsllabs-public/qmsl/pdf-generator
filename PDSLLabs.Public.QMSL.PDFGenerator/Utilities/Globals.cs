using iText.IO.Font;
using iText.IO.Font.Constants;
using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Layout;
using iText.Layout.Borders;
using System.Collections.Generic;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Utilities
{
    public class Globals
    {
        public class Constants
        {
            // BaseFont in iText7 is replaced with PdfFont
            // default font in iText7 is Helvetica with size 12 pt and color black (typically known as Normal)

            // because Arial font (located in windows default font directory) is possibly unaccessible and not supported by iText7
            // it is replaced with Helvetica as it more or less looks like Windows Arial font
            // (refer to https://www.mikesdotnetting.com/article/81/itextsharp-working-with-fonts)

            private static PdfFont NormalFont => PdfFontFactory.CreateFont(StandardFonts.HELVETICA, PdfEncodings.WINANSI, PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED);
            private static PdfFont BoldFont => PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD, PdfEncodings.WINANSI, PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED);
            public static DeviceRgb DefaultBlue => new(0, 84, 159);
            private static DeviceRgb ImageCaptionColor => new(79, 129, 189);
            public static DeviceRgb LightGreyFontColor => new(156, 158, 159);
            private static DeviceRgb ExtraLightGreyFontColor => new(236, 237, 237);
            private static DeviceRgb GreyFontColor => new(128, 128, 128);
            public static Dictionary<Orientation, PageSize> PageOrientation = new() { { Orientation.Portrait, PageSize.A4 }, { Orientation.Landscape, PageSize.A4.Rotate() } };

            public static class EvaluationReport
            {
                public static class Margins
                {
                    public static float TopMargin { get; } = 56.69f;
                    public static float RightMargin { get; } = 56.69f;
                    public static float BottomMargin { get; } = 56.69f;
                    public static float LeftMargin { get; } = 56.69f;
                }

                public static class Styles
                {
                    public static Style CoverTitle => new Style().SetFont(BoldFont).SetFontSize(25);
                    public static Style CoverSubtitle => new Style().SetFont(BoldFont).SetFontSize(20.5f);
                    public static Style CoverText => new Style().SetFont(NormalFont).SetFontSize(16f);
                    public static Style SectionHeader => new Style().SetFont(BoldFont).SetFontSize(12);
                    public static Style SubSectionHeader => new Style().SetFont(BoldFont).SetFontSize(11);
                    public static Style SectionText => new Style().SetFont(NormalFont).SetFontSize(10);
                    public static Style SectionTextBold => new Style().SetFont(BoldFont).SetFontSize(10);
                    public static Style Link => SectionText.SetFontColor(DefaultBlue);
                    public static Style TableContent => new Style().SetFont(NormalFont).SetFontSize(9);
                    public static Style TableContentBold => new Style().SetFont(BoldFont).SetFontSize(9);
                    public static Style TableHeaderBackground => new Style().SetBackgroundColor(Constants.DefaultBlue);
                    public static Style RowColorToggleBackgroundBlue => NoBorder.SetBackgroundColor(new DeviceRgb(232, 241, 250));
                    public static Style NoBorder => new Style().SetBorder(Border.NO_BORDER);
                    public static Style BorderRight => NoBorder.SetBorderRight(new SolidBorder(new DeviceRgb(232, 241, 250), 1f));
                    public static Style BorderLeft => NoBorder.SetBorderLeft(new SolidBorder(new DeviceRgb(232, 241, 250), 2f));

                }
            }

            public static class FactSheet
            {
                public static class Margins
                {
                    //ggf anpassen an den Rest?
                    public static float TopMargin { get; } = 72f;
                    public static float RightMargin { get; } = 72f;
                    public static float BottomMargin { get; } = 72f;
                    public static float LeftMargin { get; } = 72f;
                }

                public static class Styles
                {
                    public static Style Title => new Style().SetFont(BoldFont).SetFontSize(28).SetFontColor(DefaultBlue);
                    public static Style Subtitle => new Style().SetFont(BoldFont).SetFontSize(24).SetFontColor(LightGreyFontColor);
                    public static Style ChapterTitle => new Style().SetFont(BoldFont).SetFontSize(12).SetFontColor(DefaultBlue);
                    public static Style SectionTitle => new Style().SetFont(BoldFont).SetFontSize(11).SetFontColor(DefaultBlue);
                    public static Style SectionSubtitle => new Style().SetFont(NormalFont).SetFontSize(11).SetFontColor(DefaultBlue);
                    public static Style SectionSubsubtitle => new Style().SetFont(NormalFont).SetFontSize(10).SetFontColor(DefaultBlue);
                    public static Style ReportHeader => new Style().SetFont(BoldFont).SetFontSize(9).SetFontColor(DefaultBlue);
                    public static Style ReportSubHeader => new Style().SetFont(NormalFont).SetFontSize(9).SetFontColor(DefaultBlue);
                    public static Style Paragraph => new Style().SetFont(NormalFont).SetFontSize(11);
                    public static Style Footer => new Style().SetFont(NormalFont).SetFontSize(10).SetFontColor(LightGreyFontColor);
                    public static Style Header => new Style().SetFont(NormalFont).SetFontSize(12).SetFontColor(LightGreyFontColor);
                    public static Style Header_Bold => Header.SetFont(BoldFont);
                }
            }

            public static class SurveyPdf
            {
                public static class Margins
                {
                    public static float TopMargin { get; } = 56.69f;
                    public static float RightMargin { get; } = 56.69f;
                    public static float BottomMarginPortrait { get; } = 40f;
                    public static float BottomMarginLandscape { get; } = 30f;
                    public static float LeftMargin { get; } = 56.69f;

                    public static float LeftMarginImprintLandscape { get; } = 350f;
                    public static float LeftMarginImprintPortrait { get; } = 220f;
                    public static float TopMarginImprintLandscape { get; } = 240f;
                    public static float TopMarginImprintPortrait { get; } = 480f;

                    public static float TopMarginChapterTitleLandscape { get; } = 30f;
                    public static float TopMarginChapterTitlePortrait { get; } = 300f;

                    public static float TopMarginCoverLandscape { get; } = 70f;
                    public static float TopMarginCoverPortrait { get; } = 100f;

                    public static float TopMarginCover2Landscape { get; } = 50f;
                    public static float TopMarginCover2Portrait { get; } = 90f;

                    public static float FooterYPositionPortrait { get; } = 40f;
                    public static float FooterYPositionLandscape { get; } = 20f;

                    public static float SectionHeadingFirstColumnPercentagePortrait { get; } = 17f;
                    public static float SectionHeadingFirstColumnPercentageLandscape { get; } = 12f;

                    public static float PaddingBottomReportImageLandscape { get; } = 5f;
                    public static float PaddingBottomReportImagePortrait { get; } = 15f;

                    public static float MarginTableOfContentHeadlinePortrait { get; } = 50f;
                    public static float MarginTableOfContentHeadlineLandscape { get; } = 30f;

                    public static Dictionary<Orientation, Dictionary<OrientationMargins, float>> OrientationMarginValues = new()
                    {
                        { Orientation.Portrait, new Dictionary<OrientationMargins, float> {
                            { OrientationMargins.LeftMarginImprint,LeftMarginImprintPortrait },
                            { OrientationMargins.TopMarginImprint, TopMarginImprintPortrait },
                            { OrientationMargins.TopMarginChapterTitle, TopMarginChapterTitlePortrait },
                            { OrientationMargins.TopMarginCoverTitle, TopMarginCoverPortrait },
                            { OrientationMargins.TopMarginCover2Title, TopMarginCover2Portrait },
                            { OrientationMargins.FooterYPosition,FooterYPositionPortrait},
                            { OrientationMargins.BottomMargin,BottomMarginPortrait},
                            { OrientationMargins.SectionHeadingFirstColumnPercentage,SectionHeadingFirstColumnPercentagePortrait},
                            { OrientationMargins.PaddingBottomReportImage,PaddingBottomReportImagePortrait},
                            { OrientationMargins.MarginTableOfContentHeadline, MarginTableOfContentHeadlinePortrait }
                        }
                        },
                        { Orientation.Landscape, new Dictionary<OrientationMargins, float> {
                            { OrientationMargins.LeftMarginImprint,LeftMarginImprintLandscape },
                            { OrientationMargins.TopMarginImprint, TopMarginImprintLandscape },
                            { OrientationMargins.FooterYPosition,FooterYPositionLandscape},
                            { OrientationMargins.BottomMargin,BottomMarginLandscape},
                            { OrientationMargins.TopMarginChapterTitle, TopMarginChapterTitleLandscape },
                            { OrientationMargins.TopMarginCoverTitle, TopMarginCoverLandscape},
                            { OrientationMargins.TopMarginCover2Title, TopMarginCover2Landscape},
                            { OrientationMargins.SectionHeadingFirstColumnPercentage,SectionHeadingFirstColumnPercentageLandscape},
                            { OrientationMargins.PaddingBottomReportImage,PaddingBottomReportImageLandscape},
                            { OrientationMargins.MarginTableOfContentHeadline, MarginTableOfContentHeadlineLandscape }
                        }
                        }
                    };

                }

                public static class Styles
                {
                    public static Style CoverTitle => new Style().SetFont(BoldFont).SetFontSize(36).SetFontColor(DefaultBlue);
                    public static Style CoverSubtitle => new Style().SetFont(BoldFont).SetFontSize(30).SetFontColor(DefaultBlue);
                    public static Style CoverFakSubtitle => new Style().SetFont(NormalFont).SetFontSize(32).SetFontColor(DefaultBlue);
                    public static Style CoverSubsubtitle => new Style().SetFont(BoldFont).SetFontSize(26).SetFontColor(DefaultBlue);
                    public static Style ChapterCoverNumber => new Style().SetFont(NormalFont).SetFontSize(300).SetFontColor(ExtraLightGreyFontColor);
                    public static Style ChapterCoverTitle => new Style().SetFont(NormalFont).SetFontSize(18).SetFontColor(LightGreyFontColor);
                    public static Style Title => new Style().SetFont(BoldFont).SetFontSize(14).SetFontColor(DefaultBlue);
                    public static Style TOCContent => new Style().SetFont(NormalFont).SetFontSize(12f).SetFontColor(DefaultBlue);
                    public static Style Paragraph => new Style().SetFont(NormalFont).SetFontSize(11).SetFontColor(DefaultBlue);
                    public static Style ParagraphBold => new Style().SetFont(BoldFont).SetFontSize(11).SetFontColor(DefaultBlue);
                    public static Style Normal => new Style().SetFont(NormalFont).SetFontSize(10).SetFontColor(DefaultBlue);
                    public static Style Small => new Style().SetFont(NormalFont).SetFontSize(9).SetFontColor(DefaultBlue);
                    public static Style Footer => new Style().SetFont(NormalFont).SetFontSize(10).SetFontColor(GreyFontColor);
                    public static Style Explanations => new Style().SetFont(NormalFont).SetFontSize(10).SetFontColor(DefaultBlue);
                    public static Style ExplanationsHeader => Explanations.SetFont(BoldFont);
                }


            }

            public static class MeasurePdf
            {
                public static class Margins
                {
                    public static float TopMargin { get; } = 56.69f;
                    public static float RightMargin { get; } = 56.69f;
                    public static float BottomMargin { get; } = 56.69f;
                    public static float LeftMargin { get; } = 56.69f;
                    public static float PagenumberMargin { get; } = 28f;
                }

                public static class Styles
                {
                    public static Style Title => new Style().SetFont(BoldFont).SetFontSize(12);
                    public static Style Subtitle => new Style().SetFont(NormalFont).SetFontSize(11);

                    public static Style TableHeader => new Style().SetFont(BoldFont).SetFontSize(10);
                    public static Style TableContent => new Style().SetFont(NormalFont).SetFontSize(10);

                    public static Style NoBorder => new Style().SetBorder(Border.NO_BORDER);
                    public static Style BackgroundBlue => NoBorder.SetBackgroundColor(new DeviceRgb(232, 241, 250));
                    public static Style BorderLeft => NoBorder.SetBorderLeft(new SolidBorder(new DeviceRgb(232, 241, 250), 2f));
                    public static Style Footer => new Style().SetFont(NormalFont).SetFontSize(10);
                }
            }
            public static class FormalCriteriaReport
            {
                public static class Margins
                {
                    public static float TopMargin { get; } = 56.69f;
                    public static float RightMargin { get; } = 56.69f;
                    public static float BottomMargin { get; } = 56.69f;
                    public static float LeftMargin { get; } = 56.69f;
                    public static float FooterMargin { get; } = 28f;
                }

                public static class Styles
                {
                    public static Style HeadTitle => new Style().SetFont(BoldFont).SetFontSize(30).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetMarginBottom(20f).SetFontColor(DefaultBlue);
                    public static Style Title => new Style().SetFont(BoldFont).SetFontSize(20).SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER).SetMarginBottom(20f).SetFontColor(DefaultBlue);
                    public static Style Subtitle => new Style().SetFont(BoldFont).SetFontSize(11);
                    public static Style TableHeader => new Style().SetFont(BoldFont).SetFontSize(9);
                    public static Style TableContent => new Style().SetFont(NormalFont).SetFontSize(9);
                    public static Style NoBorder => new Style().SetBorder(Border.NO_BORDER);
                    public static Style BackgroundBlue => new Style().SetBackgroundColor(new DeviceRgb(141, 179, 226));
                    public static Style BorderLeft => NoBorder.SetBorderLeft(new SolidBorder(new DeviceRgb(232, 241, 250), 2f));
                    public static Style Paragraph => new Style().SetFont(NormalFont).SetFontSize(10);
                    public static Style ParagraphBold => new Style().SetFont(BoldFont).SetFontSize(10).SetFontColor(DefaultBlue);
                    public static Style Footer => new Style().SetFont(NormalFont).SetFontSize(10).SetFontColor(LightGreyFontColor);
                    public static Style Header => new Style().SetFont(NormalFont).SetFontSize(10).SetFontColor(LightGreyFontColor);
                    public static Style Header_Bold => Header.SetFont(BoldFont);
                }
            }

            public static class EvaluationCriterionPdf
            {
                public static class Margins
                {
                    public static float TopMargin { get; } = 56.69f;
                    public static float RightMargin { get; } = 56.69f;
                    public static float BottomMargin { get; } = 56.69f;
                    public static float LeftMargin { get; } = 56.69f;
                    public static float PagenumberMargin { get; } = 20f;
                }

                public static class Styles
                {
                    public static Style Category0 => new Style().SetFont(BoldFont).SetFontSize(13).SetFontColor(new DeviceRgb(255, 255, 255));
                    public static Style Category1 => new Style().SetFont(BoldFont).SetFontSize(13);
                    public static Style Category2And3 => new Style().SetFont(NormalFont).SetFontSize(10);
                    public static Style Category3Heading => new Style().SetFont(BoldFont).SetFontSize(10);
                    public static Style NoBorder => new Style().SetBorder(Border.NO_BORDER);
                    public static Style Category0Background => NoBorder.SetBackgroundColor(new DeviceRgb(66, 70, 74));
                    public static Style Category1Background => NoBorder.SetBorderBottom(new SolidBorder(new DeviceRgb(0, 0, 0), 1f));
                    public static Style Category2And3Background => NoBorder.SetBorderBottom(new SolidBorder(new DeviceRgb(178, 184, 191), 1f));
                    public static Style Footer => new Style().SetFont(NormalFont).SetFontSize(10);
                }
            }
        }

        public enum Orientation
        {
            Portrait,
            Landscape
        }

        public enum OrientationMargins
        {
            LeftMarginImprint,
            TopMarginImprint,
            TopMarginChapterTitle,
            TopMarginCoverTitle,
            TopMarginCover2Title,
            FooterYPosition,
            BottomMargin,
            SectionHeadingFirstColumnPercentage,
            PaddingBottomReportImage,
            MarginTableOfContentHeadline
        }

        public enum OutlineLevel
        {
            Level0,
            Level1,
            Level2
        }
    }
}
