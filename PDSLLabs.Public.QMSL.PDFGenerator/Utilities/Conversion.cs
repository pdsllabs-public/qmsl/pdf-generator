using iText.Html2pdf;
using iText.Layout.Element;
using System.Collections.Generic;

namespace PDSLLabs.Public.QMSL.PDFGenerator.Utilities
{
    public class Conversion
    {
        // measurement unit in PDF documents; 1 in = 25.4 mm = 72 pt
        // https://kb.itextpdf.com/home/it7kb/faq/how-to-get-the-userunit-from-a-pdf-file

        private const float unitValue = 72f;
        private const float millimitersForInch = 25.4f;

        /// <summary>
        /// Converts value from millimeters to points.
        /// </summary>
        /// <param name="millimiters">Value to convert.</param>
        /// <returns>Converted value in points.</returns>
        public static float MillimetersToPoints(float millimiters)
        {
            return millimiters / millimitersForInch * unitValue;
        }

        /// <summary>
        /// Converts html string to iText paragraph, removing tags.
        /// </summary>
        /// <param name="html">HTML content to convert.</param>
        /// <param name="preface">Boolean to switch to preface paragraph layout.</param>
        /// <returns>Converted html elements.</returns>
        public static Paragraph HTMLContentToParagraph(string html, bool preface = false)
        {
            html = @"<style> 
                        div, li { font-family:Helvetica; font-size:10pt; width:100%; }
                        img { max-width: 100%; max-height: 25cm; }
                    </style>" + html;

            Paragraph paragraph = preface ? ParagraphFactory.GetEmptyParagraph(0.5f, 10, 1.35f) : ParagraphFactory.GetEmptyParagraph(0, 9, 1.2f);

            return ConvertHtmlContent(html, paragraph);
        }

        /// <summary>
        /// Converts html string to iText paragraph, removing tags. Used specifically for the EvaluationCriterionPdf.
        /// </summary>
        /// <param name="html">HTML content to convert.</param>
        /// <returns>Converted html elements.</returns>
        public static Paragraph HTMLContentToCriterion(string html)
        {
            html = @"<style> 
                        div, li { font-family:Helvetica; font-size:10pt; width:100%; }
                        img { max-width: 100%; max-height: 17cm; }
                    </style>" + html;

            Paragraph paragraph = new();

            return ConvertHtmlContent(html, paragraph);
        }

        public static Paragraph ConvertHtmlContent(string html, Paragraph paragraph)
        {
            IList<IElement> elements = HtmlConverter.ConvertToElements(html);

            foreach (IElement element in elements)
            {
                paragraph.Add((IBlockElement)element);
                paragraph.Add(new Text("\n"));
            }

            return paragraph;
        }
    }
}