param(
	[string] $api,
	[string] $project,
	[string] $branch,
	[string] $queryTargetBranch,
	[string] $user,
	[string] $token
)

# check if mr target branch already exists
$queriedTargetBranches = Invoke-RestMethod -Headers @{ 'PRIVATE-TOKEN'="$token" } -Method Get -Uri ($api + "/projects/" + $project + "/repository/branches?search=$queryTargetBranch")
$queriedTargetBranch = $queriedTargetBranches | Sort name -desc | Select -First 1

if ($queriedTargetBranch -eq $null) {
	Write-Host "MR could not be created because no target branch like" + $queryTargetBranch + "is found"
	exit
}
$queriedTargetBranchName = $queriedTargetBranch.Name

# check if mr already exists
$openedMRs = Invoke-RestMethod -Headers @{ 'PRIVATE-TOKEN'="$token" } -Method Get -Uri ($api + "/projects/" + $project + "/merge_requests?state=opened")
$openedMR = $openedMRs | Where source_branch -eq $branch | Select -Skip 1 -First 1

if ($openedMR -eq $null) {
	# build body payload
	$body = @{
		"id"=$project;
		"source_branch"="$branch";
		"target_branch"="$queriedTargetBranchName";
		"title"="Draft: Merge $branch into $queriedTargetBranchName";
		"assignee_id"=$user;
	}

	# create mr
	$createResponse = Invoke-RestMethod -Headers @{ 'PRIVATE-TOKEN'="$token" } -ContentType "application/json" -Method Post -Uri ($api + "/projects/" + $project + "/merge_requests") -Body (ConvertTo-Json $body)
	echo "Opened a new merge request: 'Draft: $branch' and assigned to you";
	exit
}

echo "No new merge request opened";