param(
	[string] $api,
	[string] $project,	
	[string] $token
)

$queriedIntegrationBranches = Invoke-RestMethod -Headers @{ 'PRIVATE-TOKEN'="$token" } -Method Get -Uri ($api + "/projects/" + $project + "/repository/branches?search=^integration")
$queriedIntegrationBranch = $queriedIntegrationBranches | Sort name -desc | Select -First 1

$integrationBranchName = $queriedIntegrationBranch.Name

$splittedBranchName= $integrationBranchName -split '-'
$currentBranchNumber = $splittedBranchName[$splittedBranchName.Length-1]

if($currentBranchNumber -match "^\d+$")
{
	$currentBranchNumber = $currentBranchNumber -as [int]
	$currentBranchNumber++
	$answer = Invoke-RestMethod -Headers @{ 'PRIVATE-TOKEN'="$token" } -Method Post -Uri ($api + "/projects/" + $project + "/repository/branches?branch=integration/sprint-" + $splittedBranchName[$splittedBranchName.Length-2] + "-" + "{0:D2}" -f $currentBranchNumber + "&ref=dev")
	Write $answer
}
$deleteAnswer = Invoke-RestMethod -Headers @{ 'PRIVATE-TOKEN'="$token" } -Method Delete -Uri ($api + "/projects/" + $project + "/repository/merged_branches")
Write $deleteAnswer
